import React, { Component } from 'react';
import Header from '../../../component/header';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Input, Button, Div, Label, Menu, MenuTitle, Nav, TitlePage, Active, Container, H5, Main } from '../../../component';
import ButtonDelete from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import { red } from '@material-ui/core/colors';
import Footer from '../../../template/footer';
import { connect } from 'react-redux';


const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        padding: '6px 12px',
        border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: '#0063cc',
        borderColor: '#0063cc',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
})(ButtonDelete);

// untuk table


const ColorButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(red[700]),
        backgroundColor: red[700],
        '&:hover': {
            backgroundColor: red[900],
        },
    },
}))(ButtonDelete);




class KeranjangUmum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keranjang: {},
            obatList: [],
            jumlah: [],
            total: 0,
            ongkir: 0,
            bayar: 0,
            qty:[],
            laporan:{},
            statusLaporan:"Sukses",
            checked:true,
        }
    }
    nav = () => {
        if (this.props.checkLogin === true && this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin");
        } else if (this.props.checkLogin === true && this.props.user.role === "Member") {
            this.props.history.push("/dashboard");
        }
    }
    render() {
        this.nav();
        const classes = () => this.props.useStyles();

        return (
            <>
                <Container>
                    <Header />
                        <Div id="layoutSidenav">
                            <Nav>
                                <MenuTitle>
                                    Menu
                                </MenuTitle>
                                <Link to="/">
                                    <Menu icon="fas fa-tachometer-alt">
                                        Home
                                    </Menu>
                                </Link>
                                <Link to="/keranjang">
                                    <Active active="active">
                                        <Menu icon="fas fa-shopping-cart">
                                            Keranjang
                                        </Menu>
                                    </Active>
                                </Link>
                                <Link to="/login">
                                    <Menu icon="fas fa-user">
                                        Login
                                    </Menu>
                                </Link>
                            </Nav>
                            <Div id="layoutSidenav_content">
                            <Main style={{overflowY:"auto", height:"98vh"}}>
                                <Div className="container-fluid">
                                    <div className="title-table">
                                        <TitlePage icon="fas fa-shopping-cart" >
                                        
                                        </TitlePage>
                                        <div className="title">
                                            
                                            <div style={{ float: "left" }}>
                                                <h4>
                                                    Keranjang
                                                </h4>
                                            </div>
                                            {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ? 
                                            ""
                                            :
                                            <div style={{ float: "right" }}>
                                            <ColorButton
                                                variant="contained"
                                                color="primary"
                                                id="delete"
                                                className={classes.button}
                                                startIcon={<DeleteIcon />}
                                                // onClick={() => { this.hapusSemuaItem(this.state.keranjang.idKeranjang) }}
                                            >
                                                Batal Semua
                                            </ColorButton>
                                        </div>
                                        }

                                    </div>
                                    </div>

                                    <table className="table table table-striped">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>No</th>
                                                <th scope="col">Nama Obat</th>
                                                <th scope="col">Harga</th>
                                                <th width="140">Kwantitas</th>
                                                <th scope="col">Sub Total</th>
                                                {/* <th scope="col">Tanggal Registrasi</th> */}
                                                <th style={{ textAlign: "center" }} scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ?
                                                <tr>
                                                    <td
                                                        colSpan="7"
                                                        align="center"
                                                    >Data kosong</td>
                                                </tr>
                                                : <>
                                                {
                                                this.state.obatList !== null &&
                                                this.state.obatList.map((a, index) => {
                                                        return (
                                                            <tr key={index}>
                                                                <td>{index + 1}</td>
                                                                <td>{a.namaObat}</td>
                                                                <td>Rp. {a.harga}</td>
                                                                <td><Input 
                                                                type="number" 
                                                                min="1" 
                                                                className="form-control col-7" 
                                                                name="qty" 
                                                                value={this.state.qty[index]}
                                                                onChange={event=>{this.setValue(event.target.value, a.idDetailKeranjang)}} 
                                                                max={a.stok}
                                                                />
                                                                </td>
                                                                <td>Rp. {this.state.jumlah[index]}</td>
                                                                {/* <td>{a.jumlahBayar}</td> */}
                                                                {/* <td>{a.ongkir}</td> */}
                                                                <td
                                                                // className="td"
                                                                >
                                                                    <div>
                                                                        <div>
                                                                            <center>
                                                                                {this.state.obatList.length === 1?
                                                                                <Button
                                                                                // onClick={() => { this.hapusSemuaItem(this.state.keranjang.idKeranjang) }}
                                                                                className="btn btn-danger"
                                                                                >hapus</Button>
                                                                                :
                                                                                <Button
                                                                                // onClick={() => { this.hapusItem(a.idDetailKeranjang) }}
                                                                                className="btn btn-danger"
                                                                                >hapus</Button>
                                                                                }
                                                                                
                                                                            </center>
                                                                        </div>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                    }
                                                </>
                                            }
                                        </tbody>
                                    </table>
                                    {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ?
                                        <>
                                        <div>
                                            <center>
                                                <h3>Apakah anda ingin menambahkan Produk?</h3>
                                                <button className="btn btn-primary"
                                                onClick={()=>{this.props.history.push("/obat")}}>
                                                    Tambah
                                                </button>
                                            </center>
                                        </div>
                                        </>
                                        :
                                        <Div>
                                            <Div style={{ float: "right" }}>
                                            <Div style={{paddingRight:"10vh"}}>Masukkan Resep</Div>
                                            <br/>
                                            <Label className="switch">
                                                <Input 
                                                type="checkbox" 
                                                cheked={this.state.checked} 
                                                name="checked" 
                                                onChange={this.handleChecked} />
                                                <span className="slider round" />
                                            </Label>
                                        </Div>
                                        <Div className="row">
                                            <Div className="col-md-2">
                                                Total
                                            </Div>
                                            <Div className="col-md-9 row">
                                                <Div className="col-md-12">
                                                    {/* : Rp. {this.state.total} */}
                                                </Div>
                                            </Div>
                                        </Div>
                                        <Div className="row">
                                            <Div className="col-md-2">
                                                Biaya Pengiriman
                                            </Div>
                                            <Div className="col-md-9 row">
                                                <Div className="col-md-12">
                                                    {/* : Rp. {this.state.ongkir} */}
                                                </Div>
                                            </Div>
                                        </Div>
                                        <Div className="row">
                                            <Div className="col-md-2">
                                                Total Bayar
                                            </Div>
                                            <Div className="col-md-9 row">
                                                <Div className="col-md-12">
                                                    {/* : Rp. {this.state.bayar} */}
                                                </Div>
                                            </Div>
                                        </Div>
                                            <Div className="form-group" style={{ float: "left" }}>
                                                <br />
                                                <Div id="resep" style={{ float: "left" }}>
                                                    <Button className="btn btn-primary"
                                                    // onClick={()=>{this.beliProduk()}}
                                                    >Beli</Button>
                                                </Div>
                                            </Div>
                                    </Div>
                                    
                                }
                                </Div>
                            </Main>
                                <Footer>
                                    Copyright © Rilo 2021
                                </Footer>
                            </Div>
                        </Div>
                </Container>
            </>

        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    // dataUser: state.UReducer.users,
    user: state.AReducer.dataUser

})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({
    // submitLogin: (data) => dispatch({ type: "LOGIN_SUCCESS", payload: data }),

})

export default (connect(mapStateToProps, mapDispatchToProps))(KeranjangUmum);