import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Background, Layout, FormRegistration, Label, Input, FormGroup, Button, TextArea, FormRow, Div } from '../../component';
import swal from 'sweetalert';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            namaPengguna: "",
            kataSandi: "",
            nama: "",
            alamat: "",
            hp: "",
            email: "",
            konfirmasiKataSandi: ""

        }
    }
    setValue = el => {
        this.setState({
            [el.target.name]: el.target.value
        })

    }

    cancelChangePass = () => {
        swal({
            title: "Test"
        })
    }

    doRegistrasi = () => {
        const { namaPengguna, kataSandi, nama, alamat, hp, email, konfirmasiKataSandi } = this.state
        // const { dataUser } = this.props
        if (namaPengguna === "" || kataSandi === "" || nama === "" || alamat === "" || hp === "" || email === "" || konfirmasiKataSandi === "") {
            swal({
                title: "Isi semua data !!",
                icon: "warning",
                text: "Harap semua data diisi terlebih dahulu.."
            })
        } else if (kataSandi !== konfirmasiKataSandi) {
            swal({
                title: "Konfirmasi kata sandi tidak sesuai",
                icon: "warning",
                text: "Harap cek kembali kata sandi anda"
            })

        } else {
            const dataRegistrasti = {
                username: this.state.namaPengguna,
                password: this.state.kataSandi,
                name: this.state.nama,
                address: this.state.alamat,
                phone: this.state.hp,
                email: this.state.email,

            }

            fetch(`http://localhost:8080/api/registration/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify(dataRegistrasti)
            })
                .then(resp => {
                    if (!resp.ok) {
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json();
                    }
                })
                //menangkap data dari fetch
                .then(json => {
                    swal({
                        title: "Apakah kamu yakin?",
                        text: "Periksa kembali apakah data diri sudah benar?",
                        icon: "warning",
                        buttons: {
                            cancel: "Batal",
                            confirm: "Beli"
                        },

                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                swal({
                                    title: `${json.errorMessage}`,
                                    icon: "success",
                                })
                                this.props.history.push("/");
                            }
                        })
                })
                //memasukkan data dari respon json           
                .catch((e) => {
                    // alert(e);
                    // swal(e,{

                    // })

                    swal({
                        title: "perhatian",
                        text: `${e}`,
                        icon: "error",
                    });

                });
        
        
                }
    }

    nav = () => {
        if (this.props.checkLogin === true && this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin");
        } else if (this.props.checkLogin === true && this.props.user.role === "Member") {
            this.props.history.push("/dashboard");
        }
    }
    render() {
        this.nav();
        return (
            <>
                <Background>
                    <Layout>
                        <FormRegistration>
                            <Div className="form-row">
                                <FormRow>
                                    <Label className="small mb-1 required">Nama Pengguna</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="text"
                                        name="namaPengguna"
                                        placeholder="Masukkan nama pengguna"
                                        onChange={this.setValue} />
                                </FormRow>
                                <FormRow>
                                    <Label className="small mb-1 required">Nama Lengkap</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="text"
                                        name="nama"
                                        placeholder="Masukkan nama lengkap"
                                        onChange={this.setValue} />
                                </FormRow>
                            </Div>
                            <Div className="form-row">
                                <FormRow>
                                    <Label className="small mb-1 required">No. Telepon</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="number"
                                        name="hp"
                                        placeholder="Masukkan nomor telepon"
                                        onChange={this.setValue} />
                                </FormRow>
                                <FormRow>
                                    <Label className="small mb-1 required">Email</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="email"
                                        name="email"
                                        placeholder="Masukkan email"
                                        onChange={this.setValue} />
                                </FormRow>
                            </Div>
                            <FormGroup className="form-group">
                                <Label className="small mb-1 required">Alamat</Label>
                                <TextArea
                                    className="form-control"
                                    id="exampleFormControlTextarea1"
                                    rows="3"
                                    name="alamat"
                                    placeholder="Masukkan alamat"
                                    onChange={this.setValue} />
                            </FormGroup>
                            <Div className="form-row">
                                <FormRow>
                                    <Label className="small mb-1 required">Kata Sandi</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="password"
                                        name="kataSandi"
                                        placeholder="Masukkan kata sandi"
                                        onChange={this.setValue} />
                                </FormRow>
                                <FormRow>
                                    <Label className="small mb-1 required">Konfirmasi Kata Sandi</Label>
                                    <Input
                                        className="form-control py-1"
                                        type="password"
                                        name="konfirmasiKataSandi"
                                        placeholder="Masukkan kata sandi"
                                        onChange={this.setValue} />
                                </FormRow>
                            </Div>
                            <Button className="daftar" onClick={() => this.doRegistrasi()}>Daftar</Button>
                            {/* <FormGroup className="form-group mt-4 mb-0"><a className="btn btn-primary btn-block" >Daftar</a></FormGroup> */}
                            <FormGroup className="card-footer text-center">
                                <Link to="/">
                                    <FormGroup className="small">Sudah punya akun? Masuk</FormGroup>
                                </Link>
                            </FormGroup>
                        </FormRegistration>
                    </Layout>
                </Background>

            </>
        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    user: state.AReducer.dataUser


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({


})

export default (connect(mapStateToProps, mapDispatchToProps))(Register);