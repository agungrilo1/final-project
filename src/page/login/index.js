import React, { Component } from 'react';
import { Label, Input, Button, Background, Layout, Form, FormGroup, Div, I} from '../../component';
import login from '../../asset/apotek2.png';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { FaEye, FaEyeSlash } from 'react-icons/fa'; 


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pengguna: {},
            namaPengguna: "",
            kataSandi: "",
            passType: true,

        }
    }

    setValue = el => {
        this.setState({
            [el.target.name]: el.target.value
        })

    }

    doLogin = () => {
        const { namaPengguna, kataSandi } = this.state
        // const { dataUser } = this.props

        if (namaPengguna === "" || kataSandi === "") {
            swal({
                icon: "error",
                text: "Isi semua data"
            })
        } else {
            fetch(`http://localhost:8080/api/login/?username=${namaPengguna}&password=${kataSandi}`, {
                method: "get",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*",
                }
            })
                //menangkap data dari fetch
                .then(resp => {
                    if (!resp.ok) {
                        console.log(resp);
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json()
                    }
                })

                //memasukkan data dari respon json
                .then(json => {
                    this.setState({
                        pengguna: json
                    });
                    console.log("Data ", this.state.pengguna);
                    
                    if (this.state.pengguna.role === "Admin") {
                        this.props.submitLogin({ user: this.state.pengguna });
                        if (
                            this.props.checkLogin === true &&
                            this.props.user.password === "User123" 
                         ) {
                            swal({
                                icon: "warning",
                                title: "Perhatian",
                                text: `Anda harus mengubah kata sandi terlebih dahulu`
                            }); 
                            this.props.history.push("/password");
                         }else{
                            swal({
                                icon: "success",
                                title: "Berhasil Masuk",
                                text: `Selamat Datang ${this.state.pengguna.name}`
                            });
                            this.props.history.push("/dashboard-admin");
                        }
                    } else if (this.state.pengguna.role === "Member") {
                        this.props.submitLogin({ user: this.state.pengguna });
                        if (
                            this.props.checkLogin === true &&
                            this.props.user.password === "User123" 
                         ) {
                            swal({
                                icon: "warning",
                                title: "Perhatian",
                                text: `Anda harus mengubah kata sandi terlebih dahulu`
                            });
                             this.props.history.push("/password");
                         }else{
                            swal({
                                icon: "success",
                                title: "Berhasil Masuk",
                                text: `Selamat Datang ${this.state.pengguna.name}`
                            });
                            this.props.history.push("/dashboard");
                         }
                        
                    }

                })
                .catch((e) => {
                    swal({
                        title: `${e}`,
                        icon: "warning"
                    });

                });
            
        }
        console.log("Data", this.state.pengguna)
    };

    passClick = () => {
        console.log("pass");
        const passTypeTemp = this.state.passType
        this.setState({
            passType: !this.state.passType
        })

    }


    nav = () => {
            if (
                this.props.checkLogin === true && 
                    (
                        this.props.user.password === "User123" 
                        ||
                        this.props.user.password === "Admin123" 
                    )
                ) {
                    this.props.history.push("/password");
                } else 
            if (
                this.props.checkLogin === true &&
                this.props.user.role === "Member"
            ) {
                this.props.history.push("/dashboard");
            }else if( 
                this.props.checkLogin === true &&
                this.props.user.role === "Admin"
            ) {
                this.props.history.push("/dashboard-admin");
            }
    }


    render() {
        this.nav();
        

        return (
            <Background>
                <Layout>
                    <Form>
                        <FormGroup className="form-group">
                            <Label className="small mb-1">Nama Pengguna</Label>
                            <Input className="form-control py-4"
                                name="namaPengguna"
                                type="text"
                                placeholder="Masukkan Nama Pengguna"
                                onChange={this.setValue} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label className="small mb-1">Kata Sandi</Label>
                            <Input 
                            className="form-control py-4"
                                type={this.state.passType ? "password" : "text" }
                                name="kataSandi"
                                id="password-field"
                                placeholder="Masukkan Kata Sandi"
                                onChange={this.setValue} 
                                />
                                <Button 
                                    className="show-pasword " 
                                    style={{ 
                                    border: "none", 
                                    textDecoration:"none",
                                    backgroundColor : "Transparent",
                                    backgroundRepeat: "no-repeat",
                                    border:"none",
                                    cursor:"pointer",
                                    overflow: "hidden",
                                    outline:"none",
                                    float: "right",
                                    marginLeft: -25,
                                    marginTop: -35,
                                    position: "relative",
                                    zIndex: 2}} onClick={()=>{this.passClick()}} > 
                                    {/* {this.state.passType ? <i className='fa fa-eye-slash'></i> : <i className= 'fa fa-eye'></i> } */}
                                    {this.state.passType ?  <FaEyeSlash/> : <FaEye/>  }
                                </Button>
                                 
                                
                                
                        </FormGroup>
                        <FormGroup className="d-flex flex-row-reverse">
                        {/* <div style={{marginLeft="50vh"}}> */}
                            <Button id="masuk" className="btn btn-primary " onClick={() => this.doLogin()}>Masuk</Button>

                        {/* </div> */}
                        </FormGroup>
                        <FormGroup className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                            <Link to="/register">
                                <Div style={{ display: "flex", marginLeft: "12rem" }}>Belum punya akun? Registrasi!</Div>
                            </Link>
                        </FormGroup>
                    </Form>
                </Layout>
            </Background>
        );
    }
}


//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser

})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({
    submitLogin: (data) => dispatch({ type: "LOGIN_SUCCESS", payload: data }),

})

export default (connect(mapStateToProps, mapDispatchToProps))(Login);