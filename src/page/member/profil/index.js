import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, MenuTitle, Nav, Active, Button, Div, Label, Input } from '../../../component';
import { Footer } from '../../../template';
import Header from '../../../component/header';
import user from '../../../asset/user.png';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import $ from 'jquery';
import { FaEye, FaEyeSlash } from 'react-icons/fa'; 

class Profil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profilPengguna: {},
            id:this.props.login.idUser,
            nama: "",
            alamat: "",
            namaPengguna: "",
            hp: "",
            kataSandi: "",
            kataSandiLama: "",
            konfirmasiKataSandi: "",
            idubah : true,
            disabled: false,
            passType: true,
        }
    }
    clearField = ()=>{
        this.setState({
            nama: "",
            alamat: "",
            hp:"",
            namaPengguna: "",
            email: "",
            idubah : false
            
        })
        
    }

    setValue = el => {
        this.setState({
            [el.target.name]: el.target.value,
            disabled: !this.state.disabled
        })

    }
    getProfil = () => {
        console.log("ini id profil", this.props.user.idUser);
        let id = this.props.user.idUser;
        fetch(`http://localhost:8080/api/profil/?idUser=${encodeURIComponent(id)}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    profilPengguna: json
                });
                this.nav();
                if (typeof json.errorMessage !== 'undefined') {
                    swal("Gagal !", json.errorMessage, "error");
                }
            })
            .catch((e) => {
                console.log(e);
                swal("Gagal !", "Gagal mengambil data", "error");
            });
    };

    componentDidMount() {
        this.getProfil();
    }

   
    doChangePass = () => {
        let object = this.state;
        let regKataSandi = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*[&%$]).{6,8}$/;
        console.log("pas :", regKataSandi.test(object.kataSandi));
        const regpas = regKataSandi.test(object.kataSandi);
        //true lanjut
        //false block
        if (object.kataSandiLama === "" || object.kataSandi === "" || object.konfirmasiKataSandi === "") {
            swal({
                title: "Gagal !!",
                text: "Kata sandi baru dan konfirmasi kata sandi harus diisi",
                icon: "warning"
            })
        } else if(object.kataSandiLama !== this.props.user.password){
            swal({
                title: "Gagal !!",
                text: "Kata sandi lama tidak sesuai",
                icon: "warning"
            }) 
        } else if(object.kataSandi === this.props.user.password){
            swal({
                title: "Gagal !!",
                text: "Tidak boleh menggunakan kata sandi lama",
                icon: "warning"
            }) 
        } else if (regpas === false) {
            swal({
                title: "Gagal !!",
                text: "Kata sandi harus terdiri dari 6 sampai 8 alfanumerik dan minimal 1 huruf kapital",
                icon: "warning"
            })
        }else if (object.kataSandi !== object.konfirmasiKataSandi) {
            swal({
                title: "Gagal !!",
                text: "konfirmasi kata sandi tidak sesuai dengan kata sandi baru",
                icon: "warning"
            })
        } else {
            swal({
                title: "Apakah anda yakin ingin mengganti kata sandi",
                text: "Setelah anda mengubah kata sandi anda akan diarahkan ke halaman awal masuk",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Kata sandi berhasil diubah", {
                            icon: "success",
                        });
                        const changePass = {
                            idUser: this.props.user.idUser,
                            password: this.state.kataSandi
                        }
                        fetch(`http://localhost:8080/api/change-password/`, {
                            method: "put",
                            headers: {
                                "Content-Type": "application/json; ; charset=utf-8",
                                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                                "Access-Control-Allow-Origin": "*"
                            },
                            body: JSON.stringify(changePass)
                        })
                            .then((response) => response.json())
                            .then((json) => {
                                console.log("ini json", json);
                                console.log("ini json", json.errorMessage);
                                if (json.errorMessage === "undefined") {
                                    swal("Gagal !", json.errorMessage, "error");

                                } else {

                                    // this.props.history.push("/profil")
                                    // this.props.logoutEvent()
                                    
                                    swal({
                                        icon: "success",
                                        title: "Berhasil"
                                    })
                                    .then((value) => {
                                        $("#kata-sandi .close").click();
                                            this.props.logoutEvent();
                                            this.props.history.push("/")
                                        })
                                }
                            })
                            .catch((e) => {
                                console.log(e);
                                swal("Gagal !", "Gagal mengambil data", "error");
                            });
                    }
                });

        }
    }

    passClick = () => {
        console.log("pass");
        const passTypeTemp = this.state.passType
        this.setState({
            passType: !this.state.passType
        })

    }

    profileValue = () => {
        //Memasukkan value ke dalam input update
        this.setState({
            nama: this.state.profilPengguna.name,
            alamat: this.state.profilPengguna.address,
            hp: this.state.profilPengguna.phone,
            namaPengguna: this.state.profilPengguna.username,
            email: this.state.profilPengguna.email
        })
    }

    doChangeProfile = () => {
        let obj = this.state

        if (
            obj.nama === "" ||
            obj.alamat === "" ||
            obj.hp === "" ||
            obj.namaPengguna === "" ||
            obj.email === ""
        ) {
            swal({
                title: "Gagal !!",
                text: "Isi semua field",
                icon: "warning"
            })
        }
        else {
            
            const changeProfil = {
                idUser: this.props.user.idUser,
                name: this.state.nama,
                address: this.state.alamat,
                phone: this.state.hp,
                email: this.state.email,
                username: this.state.namaPengguna
            }
            console.log("iniiii" , changeProfil);
            fetch(`http://localhost:8080/api/change-profile/`, {
                method: "put",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify(changeProfil)
            })
                .then(resp => {
                    if (!resp.ok) {
                        console.log(resp);
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json()
                    }
                })
                .then(json => {
                    
                    if (typeof json.errorMessage === 'undefined') {
                        swal("Gagal !", json.errorMessage, "error");
                    } else{
                        swal({
                            icon: "success",
                            title: "Berhasil"
                        })
                        .then((value) => {
                            this.getProfil();
                            // document.querySelector('#profil').modal('hide');
                            // this.clearField();
                            $("#profil .close").click();
                            
                        })
                    }
                    
                    
                })
                .catch((e) => {
                    console.log(e);
                    swal({
                        title:`${e}`,    
                    });
                });
        }
    }

    nav=()=>{
        if (this.props.checkLogin === false ) {
            this.props.history.push("/");
        } else if(this.props.checkLogin === true &&
            this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin")
        }
      } 
    render() { 
        this.nav();
        
        const { id, nama, hp, alamat, email, namaPengguna, profilPengguna}= this.state
        // const { idubah} = this.state
        return (
            <>
                <Div className="sb-nav-fixed">

                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                            </MenuTitle>
                            <Link to="/dashboard">
                                <Menu icon="fas fa-tachometer-alt">
                                Beranda
                                </Menu>
                            </Link>
                            <Link to="/obat">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                    Obat
                                </Menu>
                            </Link>
                            <Link to="/keranjang-member">
                                <Menu icon="fas fa-shopping-cart">
                                    Keranjang
                                </Menu>
                            </Link>
                            <Link to="/profil">
                                <Active active="active">
                                    <Menu icon="fas fa-user">
                                        Profil
                                </Menu>
                                </Active>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <main>
                                <Div className="profil ml-4 mt-5">
                                    <Div className="kiri">
                                        <img src={user} className="user" />
                                        <br />
                                        <Div className="update-profil">
                                            <Button
                                                dataToogle="modal"
                                                dataTarget="#profil"
                                                id="sunting"
                                                className="btn btn-warning"
                                                onClick={() => this.profileValue()}
                                                data-target="#exampleModal" >
                                                Sunting Profil
                                            </Button>
                                        </Div>
                                        <Div className="change-password">
                                            <Button
                                                dataToogle="modal"
                                                dataTarget="#kata-sandi"
                                                id="sunting"
                                                className="btn btn-primary"
                                                data-target="#exampleModal" >
                                                Ubah Kata Sandi
                                            </Button>
                                        </Div>
                                    </Div>
                                    <Div className="kanan">
                                        <Div className="profil-pengguna">
                                            Profil Pengguna
                                        </Div>
                                        <table width="100%" align="left" cellSpacing={1} cellPadding={5}>
                                            <tbody>
                                                <tr>
                                                    <td>ID</td>
                                                    <td>:</td>
                                                    <td>{profilPengguna.idUser}</td>
                                                </tr>
                                                <tr>
                                                    <td>Username</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.username}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.name}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.address}</td>
                                                </tr>
                                                <tr>
                                                    <td>Telepon</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.phone}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.email}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Transaksi</td>
                                                    <td>:</td>
                                                    <td>{this.state.profilPengguna.registrationDate}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Div>
                                </Div>
                            </main>
                            <Footer>
                                Copyright © Rilo 2021
                            </Footer>
                        </Div>
                    </Div>
                </Div>
                {/* Modal */}
                <div className="modal fade" id="profil" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Profil</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <Div className="modal-body">
                                <Label className="small mb-1">ID User</Label>
                                <Input
                                 value={id}
                                 type="text"
                                 placeholder="ID User"
                                 className="form-control"
                                 disabled={true}/>    
                                <Label className="small mb-1">Nama Pengguna</Label>
                                <Input
                                    type="text"
                                    name="namaPengguna"
                                    className="form-control"
                                    placeholder="Masukkan Nama Pengguna"
                                    value={namaPengguna}
                                    onChange={this.setValue} />
                                <Label className="small mb-1">Nama Lengkap</Label>
                                <Input
                                    type="text"
                                    name="nama"
                                    className="form-control"
                                    placeholder="Masukkan Nama"
                                    value={nama}
                                    onChange={this.setValue} />
                                <Label className="small mb-1">Alamat</Label>
                                <textarea
                                    type="text"
                                    name="alamat"
                                    className="form-control"
                                    placeholder="Masukkan Alamat"
                                    value={alamat}
                                    onChange={this.setValue} />
                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <Label className="small mb-1">Telepon</Label>
                                            <Input
                                                type="number"
                                                name="hp"
                                                min="0"
                                                className="form-control"
                                                placeholder="Masukkan Nomor Telepon"
                                                value={hp}
                                                onChange={this.setValue} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <Label className="small mb-1">Email</Label>
                                            <Input
                                                type="text"
                                                name="email"
                                                className="form-control"
                                                placeholder="Masukkan Email"
                                                value={email}
                                                onChange={this.setValue} />
                                        </div>
                                    </div>
                                </div>

                            </Div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <Button type="button" className="btn btn-primary" onClick={() => this.doChangeProfile()}>Simpan</Button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Modal */}
                <div className="modal fade" id="kata-sandi" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Ubah Kata Sandi</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <Label >Kata Sandi Lama</Label>
                                <Input type={this.state.passType ? "password" : "text" } name="kataSandiLama" className="form-control" placeholder="Masukkan Kata Sandi Lama" onChange={this.setValue} />
                                
                                <Label >Kata Sandi Baru</Label>
                                <Input type="password" name="kataSandi" className="form-control" placeholder="Masukkan Kata Sandi" onChange={this.setValue} />
                                <Label>Konfirmasi Kata Sandi Baru</Label>
                                <Input type="password" name="konfirmasiKataSandi" className="form-control" placeholder="Konfirmasi Kata Sandi" onChange={this.setValue} />

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <Button type="button" className="btn btn-primary" onClick={() => this.doChangePass()}>Simpan</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}



//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    login: state.AReducer.dataUser,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser,
    checkLogin: state.AReducer.isLogin,

})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({

    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })
})

export default (connect(mapStateToProps, mapDispatchToProps))(Profil);