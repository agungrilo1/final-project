import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../../../component/header';
import { Menu, ModalHeader, Button, Active, Nav, MenuTitle, Div, Modal, Input,Tooltip, I } from '../../../component';
import { connect } from 'react-redux';
import gambar from '../../../asset/obat.jpg';
import { Footer } from '../../../template';
import Pagination from '@material-ui/lab/Pagination';
import swal from 'sweetalert';
import $ from 'jquery';
import ReactTooltip from 'react-tooltip';
import konversiRupiah from '../../../component/util';

class ObatMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idObat: "",
            namaObat: "",
            stok: "",
            harga: "",
            dosis: "",
            idJenisObat: "",
            jenisObat: "",
            limit: 8,
            page: 1,
            count: 0,
            obat: [],
            cari: "",
            kondisiCari:false,
            total: "",
            keranjang: {},
            obatList: [],
            resep: ""
        }
    }

    setValue = el => {
        if(el.target.name === "cari" && el.target.value === ""){
            this.getObatListWithPage();
            this.getCount();
        }

        this.setState({
            [el.target.name]: el.target.value,
        })
        console.log("ini value", el.target.value);

    }


    //pagination
    getCount = () => {
        fetch(`http://localhost:8080/api/count/?idUser=${this.props.user.idUser}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                
                // let dataObat = this.state.obat.filter((x) => x.statusObat === "Active");
                this.setState({
                    count: Math.ceil(Number(json) / this.state.limit)
                });

            })
            .catch((e) => {
                alert(e);
            });
    }

    getTotal = () => {
        fetch('http://localhost:8080/api/count/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    total: json
                });
                // console.log("count", this.state.count);
                // console.log("count", Number(json));
            })
            .catch((e) => {
                alert(e);
            });
    }

    handleChange = (event, value) => {
        this.setState({
            page: value
        })
        if(this.state.cari !== ""){
            this.searchPagination(value, this.state.limit)
        }else{
            this.getObatListWithPage(value, 8);
            this.getCount();
        }
    }

    getKeranjang = () => {
        let id = this.props.user.idUser;

        fetch(`http://localhost:8080/api/keranjang/?idUser=${id}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {

                this.setState({
                    keranjang: json
                });


                if ($.isEmptyObject(this.state.keranjang) === true) {
                    this.setState({
                        obatList: []
                    });
                } else {
                    this.setState({
                        obatList: this.state.keranjang.obatList
                    });
                }

                // }
            })
            .catch((e) => {
                console.log(e);
            });

    };


    addToCart = () => {
        let id = this.props.user.idUser;
        let idObat = this.state.idObat
        let cek;
        this.getKeranjang()
        console.log("obat list idKeranjang", this.state.obatList);
        if ($.isEmptyObject(this.state.keranjang) === true) {
            const objAdd = {
                resep: this.state.resep,
                idUser: id,
                idObat: idObat
            }
            fetch(`http://localhost:8080/api/create-keranjang/?idUser=${id}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify(objAdd)
            })
                .then(resp => {
                    if (!resp.ok) {
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json();

                    }
                })
                //menangkap data dari fetch
                .then(json => {

                    console.log("id keranjang", this.state.keranjang.idKeranjang);

                    swal({
                        title: `${json.errorMessage}`,
                        icon: "success",
                    })

                        .then((value) => {
                            $("#obat .close").click();
                            this.getKeranjang()
                        })
                })
                .catch((e) => {
                    swal({
                        title: "perhatian",
                        text: `${e}`,
                        icon: "error",
                    });
                    console.log("ini", e);

                });

        } else {
            const objAdd2 = {
                resep: this.state.resep,
                idUser: id,
                idObat: idObat
            }
            for (let i = 0; i < this.state.obatList.length; i++) {
                if (this.state.obatList[i].idObat === objAdd2.idObat) {
                    cek = true;
                    console.log("obat list idKeranjang", this.state.obatList);
                    break;
                } else {
                    cek = false;
                }
            }
            if (cek === false) {
                this.createDetail();
            } else {
                swal({
                    title: "Produk sudah ada di dalam keranjang",
                    icon: "warning",
                })
                    .then((value) => {
                        $("#obat .close").click();
                    })

            }
        }

    }

    createDetail = () => {
        const detail = {
            idObat: this.state.idObat,
            idKeranjang: this.state.keranjang.idKeranjang,
        }

        fetch(`http://localhost:8080/api/create-detail/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify(detail)
        })
            .then(resp => {

                if (!resp.ok) {
                    return resp.json().then(text => {
                        throw new Error(`${text.errorMessage}`)
                    })
                } else {
                    return resp.json();

                }
            })
            //menangkap data dari fetch
            .then(json => {
                swal({
                    title: `${json.errorMessage}`,
                    icon: "success",
                })
                    .then((value) => {
                        // this.clearField();
                        $("#obat .close").click();
                        // this.get();
                        // this.getKeranjang();

                    })
            })
            .catch((e) => {
                swal({
                    title: "perhatian",
                    text: `${e}`,
                    icon: "error",
                });


            });

    }

    getObatListWithPage = (page, limit) => {
        if (page){}else { page = this.state.page }
        if(limit){}else{limit = this.state.limit}
          
        fetch('http://localhost:8080/api/obat-pagination/?page=' + page + '&limit=' + limit + '', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ obat: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    searchPagination = (page, limit) => {
        if(this.state.cari === ""){
            swal({
                title: "Perhatian",
                icon: "warning",
                text: "Harap masukkan kata pencarian"
            })
        }else{
            this.setState({
                kondisiCari : true
            })
        let keyword = this.state.cari
        if (page){}else { page = 1 }
        Promise.all([
            fetch(`http://localhost:8080/api/search/obat/?idUser=${this.props.user.idUser}&keyword=${keyword}&page=${1}&limit=${this.state.limit}`),
            fetch('http://localhost:8080/api/search-counter/obat/?idUser='+this.props.user.idUser+'&keyword=' + keyword),
        ])
            .then(([response, response2]) => Promise.all([response.json(), response2.json()]))
            .then(([json, json2]) => {
                console.log(Math.ceil(Number(json2) / this.state.limit));
                console.log(parseInt(json2));
                console.log(json2);
                this.setState({
                    obat: json, 
                    count: Math.ceil(Number(json2.countSearch) / this.state.limit),
                    page:1
                })
                console.log(json);
                console.log("ini count", this.state.count);
                console.log("count", this.state.count);
            })
        }
    }

    componentDidMount() {
        this.getKeranjang();
        this.getCount();
        this.getObatListWithPage();
        this.getTotal();
        // this.getJenisObat();
    }


    //detail obat
    detailObatValue = (idObat) => {
        console.log(idObat);
        const cari = this.state.obat.findIndex((idx) => idx.idObat === idObat);
        const detail = this.state.obat[cari];
        console.log(detail);
        this.setState({
            idObat: idObat,
            namaObat: detail.namaObat,
            harga: detail.harga,
            stok: detail.stok,
            jenis: detail.jenis,
            dosis: detail.dosis,
            idJenisObat: detail.idJenisObat,

        });
        console.log("dasdsd", this.state.idObat);
    }


    nav = () => {
        if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin")
        }
    }
    render() {
        this.nav();
        return (
            <>
                <Div className="sb-nav-fixed">
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                        </MenuTitle>
                            <Link to="/dashboard">
                                <Menu icon="fas fa-tachometer-alt">
                                    Beranda
                            </Menu>
                            </Link>
                            <Link to="/obat">
                                <Active active="active">
                                    <Menu icon="fas fa-prescription-bottle-alt">
                                        Obat
                                </Menu>
                                </Active>
                            </Link>
                            <Link to="/keranjang-member">
                                <Menu icon="fas fa-shopping-cart">
                                    Keranjang
                            </Menu>
                            </Link>
                            <Link to="/profil">
                                <Menu icon="fas fa-user">
                                    Profil
                            </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <main>
                                <Div className="container-fluid">
                                    <Div className="row mt-3 ">
                                        <Div className="col-9 mb-2 " 
                                            style={{ padding: "inherit", 
                                            marginLeft: "2vh" }}>
                                        </Div>
                                        <Input
                                            type="search"
                                            name="cari"
                                            className="form-control col-md-8 ml-2"
                                            placeholder="Cari Obat"
                                            onChange={this.setValue}
                                            
                                        />
                                        <Button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => { this.searchPagination() }}
                                        >
                                            <i className="fas fa-search" />
                                        </Button>
                                    </Div>
                                    <Div className="row">
                                        {this.state.obat.length === 0 ?
                                        <Div>

                                            <h1 className="ml-5 mt-3" style={{textAlign:"center"}}>Obat Tidak Ditemukan <I className="fas fa-exclamation-triangle"/></h1>
                                            
                                        </Div>
                                        
                                        :
                                        this.state.obat.filter(value => value.statusObat === "Active").map((a, index) => {
                                            return (
                                                <>
                                                    <Div className="col-xl-3" key={index} >
                                                        <Div className="card obat">
                                                            <img 
                                                                src={gambar} 
                                                                width={254} 
                                                                height={120} 
                                                                alt="Obat" />

                                                            <Div className="card-body" style={{ height: "10px" }}>
                                                                <p className="mt-0 text-success">{a.namaObat} {a.stok === 0 &&
                                                                    <span className="badge badge-danger">Habis</span>}
                                                                </p>

                                                                <p className="card-text">
                                                                    Harga : {konversiRupiah(a.harga)}
                                                                </p>
                                                            </Div>
                                                            <Div style={{ marginLeft: 200, marginBottom: 10 }}>
                                                                <Tooltip isi="Detail">

                                                                    <Button
                                                                        className="btn btn-secondary"
                                                                        dataToogle="modal"
                                                                        dataTarget="#obat"
                                                                        data-target="#exampleModal"
                                                                        onClick={() => { this.detailObatValue(a.idObat) }}
                                                                        >
                                                                        <i className="fas fa-info-circle"></i>
                                                                    </Button>
                                                                </Tooltip>
                                                                <ReactTooltip/>
                                                            </Div>
                                                        </Div>
                                                    </Div>


                                                </>

                                            )
                                        })
                                        }
                                        {this.state.obat.length === 0 ?
                                        <Div></Div>
                                    :
                                    <Div
                                            className="pagination" style={{ marginTop: "10vh", marginBottom: "1vh" }}
                                        >
                                            {/* <Div
                                                style={{
                                                    marginLeft: 90,
                                                    marginTop: 10
                                                }}
                                            >
                                                
                                            </Div> */}
                                            <Div style={{marginTop:"2vh"}}>
                                                <Pagination count={this.state.count} page={this.state.page}
                                                    onChange={this.handleChange}
                                                />
                                            </Div>

                                        </Div>
                                    }
                                        

                                    </Div>

                                </Div>
                            </main>
                            {/* <Footer>
                            Copyright © Rilo 2021
                            </Footer> */}
                        </Div>
                    </Div>
                </Div>
                <Modal id="obat">
                    <ModalHeader className="close">
                        Detail Obat
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-2">
                                    ID Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.idObat}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Nama Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.namaObat}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Stok
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.stok}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Harga
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {konversiRupiah(this.state.harga)}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Jenis Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.jenis}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Dosis
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.dosis}
                                    </Div>
                                </Div>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        {this.state.stok === 0 ?
                            <Button type="button" disabled="disabled" className="btn btn-warning text-white" >
                                <i className="fas fa-cart-plus"></i> Stok Habis
                        </Button>
                            :
                            <Button type="button" className="btn btn-warning text-white" onClick={() => this.addToCart(this.state.idObat)}>
                                <i className="fas fa-cart-plus"></i> Masukkan kedalam keranjang
                        </Button>
                        }

                    </Div>
                </Modal>
            </>
        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    user: state.AReducer.dataUser


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({


})

export default (connect(mapStateToProps, mapDispatchToProps))(ObatMember);