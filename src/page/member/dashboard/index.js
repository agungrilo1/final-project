import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Footer } from '../../../template';
import { connect } from 'react-redux';
import { Menu, MenuTitle, Nav, Active, Container, H3, Div, I, Button, Tooltip, Modal, ModalHeader } from '../../../component';
import Header from '../../../component/header';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import ReactTooltip from 'react-tooltip';
import $ from 'jquery';
import konversiRupiah from '../../../component/util';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            record: {},
            riwayatPembelian: [],
            laporan: [],
            obatList: [],
            idLaporan: "",
            nama: "",
            tanggalTransaksi: "",
            tanggalKonfirmasi: "",
            total: 0,
            ongkir: 0,
            harga: 0,
            status: "",
            display:"none",

            selected: this.handleSetDate(new Date())
        }
        this.handleDateChange = (date) => {
            let dateValue = this.handleSetDate(date)

            this.setState({
                selected: dateValue
            }, () => this.getRecord(dateValue), this.getRiwayat(dateValue))
        }
    }

    handleSetDate = dateValue => {
        let current_datetime;
        if (dateValue === null) {
            dateValue = new Date()
            current_datetime = dateValue
        } else {
            current_datetime = dateValue
        }
        let year = current_datetime.getFullYear();
        let month = ("0" + (current_datetime.getMonth() + 1)).slice(-2)
        let day = ("0" + current_datetime.getDate()).slice(-2)
        let formatted_date = year + "-" + month + "-" + day
        console.log("cek", year, month, day, formatted_date);
        console.log("ini tanggal", (current_datetime.getDate()));
        return formatted_date
    }

    getRecord = (formatted_date) => {
        fetch(`http://localhost:8080/api/beranda-member/?idUser=${this.props.user.idUser}&tglTransaksi=${formatted_date}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ record: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    getRiwayat = formatted_date => {
        fetch(`http://localhost:8080/api/riwayat-beranda/?idUser=${this.props.user.idUser}&tglTransaksi=${formatted_date}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ riwayatPembelian: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    closeBtn = () => {
        $("#obat .close").click();
    }


    detailLaporanValue = (idLaporan) => {
        const cari = this.state.riwayatPembelian.findIndex((idx) => idx.idLaporan === idLaporan);
        const detail = this.state.riwayatPembelian[cari];

        this.setState({
            idLaporan: detail.idLaporan,
            nama: detail.name,
            tanggalTransaksi: detail.tglTransaksi,
            tanggalKonfirmasi: detail.tglKonfirmasi,
            status: detail.statusLaporan,
            bayar: detail.total,
            ongkir: detail.ongkir,
            obatList: detail.obatList
        });
    }

    nav = () => {
        if (this.props.user.password === "User123") {
            this.props.history.push("/password")
        } else if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin")
        }
    }

    closeInfo=()=>{
        this.setState({
            display:""
        })
    }

    componentDidMount = () => {
        let dateValue = this.handleSetDate(new Date())
        this.getRecord(dateValue);
        this.getRiwayat(dateValue);

    }
    render() {
        this.nav();
        return (
            <>
                <Container>
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                            </MenuTitle>
                            <Link to="/dashboard">
                                <Active active="active">
                                    <Menu icon="fas fa-tachometer-alt">
                                        Beranda
                                </Menu>
                                </Active>
                            </Link>
                            <Link to="/obat">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                    Obat
                                </Menu>
                            </Link>
                            <Link to="/keranjang-member">
                                <Menu icon="fas fa-shopping-cart">
                                    Keranjang
                                </Menu>
                            </Link>
                            <Link to="/profil">
                                <Menu icon="fas fa-user">
                                    Profil
                                </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Div id="alert" className="alert alert-info alert-dismissible fade show" style={{}}  role="alert">
                                <strong>Perhatian !!</strong> Pada halaman ini berisikan laporan pembelian berupa jumlah obat dan biaya pembelian dalam seminggu terakhir
                                <button type="button" className="close" onClick={()=>{this.closeInfo()}} data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </Div>
                            <Div className="container-fluid" >
                                <Div className="container-fluid" style={{ backgroundColor: "gray", borderRadius: "10vh", color: "white", display:this.state.display}}>
                                    <h2 className="title-dashboard-member">SELAMAT DATANG</h2>
                                </Div>
                                <Div className="container">
                                </Div>
                                <center>
                                    <Div>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                // value={this.handleSetDate(new Date())}
                                                value={(this.state.selected)}
                                                onChange={date => this.handleDateChange(date)}
                                                format="yyyy-MM-dd"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Div>
                                </center>
                                <Div className="row justify-content-md-center">
                                    <Div className="col-xl-4">
                                        <Div className="card obat m-b-30">
                                            <I className="fas fa-prescription-bottle-alt fa-3x mt-3" style={{ alignSelf: "center" }}></I>
                                            <Div className="card-body">
                                                <h5 className="text-center font-16 text-success mt-3">Jumlah Obat</h5>
                                                {this.state.record.totalObat === 0 ?
                                                    <h2 style={{ textAlign: "center" }}>0</h2>
                                                    :
                                                    <h2 className="text-center">{this.state.record.totalObat}</h2>
                                                }
                                            </Div>
                                        </Div>
                                    </Div>
                                    <Div className="col-xl-4">
                                        <Div className="card obat m-b-30">

                                            <I className="fas fa-money-bill-alt fa-3x mt-3" style={{ alignSelf: "center" }}></I>
                                            <Div className="card-body mt-3">
                                                <h5 className="font-16 text-center text-success">Jumlah Pengeluaran</h5>
                                                {this.state.record.jumlahPengeluaran === 0 ?
                                                    <h2 style={{ textAlign: "center" }}>Rp. 0</h2>
                                                    :
                                                    <h2 style={{ textAlign: "center" }}>{this.state.record.jumlahPengeluaran && konversiRupiah(this.state.record.jumlahPengeluaran)}</h2>
                                                }
                                            </Div>
                                        </Div>
                                    </Div>
                                    <Div
                                        className="table-wrapper-scroll-y my-custom-scrollbar mt-3 scroll"
                                    >

                                        <table cellSpacing="0" className="table table-bordered table-sm">
                                            <thead className="position-">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Tanggal Konfirmasi</th>
                                                    <th>Status</th>
                                                    <th>Total Pembayaran</th>
                                                    <th>Detail</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                                {this.state.riwayatPembelian.length === 0 ?
                                                    <tr>
                                                        <td
                                                            colSpan="5"
                                                            align="center"
                                                        >Data kosong</td>
                                                    </tr>
                                                    :
                                                    this.state.riwayatPembelian.map((riwayat, index) => {
                                                        return (
                                                            <tr key={index} >
                                                                <td>{index + 1}</td>
                                                                <td>{riwayat.tglTransaksi}</td>

                                                                {riwayat.tglKonfirmasi !== null ? <td> {riwayat.tglKonfirmasi}</td> : <td >Menunggu Konfirmasi</td>}
                                                                {riwayat.statusLaporan === "Menunggu" &&
                                                                    <td><span className="badge badge-secondary">{riwayat.statusLaporan}</span></td>
                                                                }
                                                                {riwayat.statusLaporan === "Sukses" ?
                                                                    <td><span className="badge badge-success">{riwayat.statusLaporan}</span></td>
                                                                    :
                                                                    riwayat.statusLaporan === "Gagal" &&
                                                                    <td><span className="badge badge-danger">{riwayat.statusLaporan}</span></td>
                                                                }

                                                                <td>{konversiRupiah(riwayat.total)}</td>
                                                                <td>
                                                                    <center>
                                                                        <Tooltip isi="Detail">
                                                                            <Button
                                                                            // style={{margin:"0"}}
                                                                                className="btn btn-secondary"
                                                                                dataToogle="modal"
                                                                                dataTarget="#obat"
                                                                                data-target="#exampleModal"
                                                                                onClick={() => { this.detailLaporanValue(riwayat.idLaporan) }}
                                                                            >
                                                                                <I className="fas fa-info-circle" />
                                                                            </Button>
                                                                        </Tooltip>
                                                                        <ReactTooltip />
                                                                    </center>
                                                                </td>

                                                            </tr>
                                                        )
                                                    })}

                                            </tbody>
                                        </table>
                                    </Div>
                                </Div>
                            </Div>
                            <Footer>
                                Copyright © Rilo 2021
                        </Footer>
                        </Div>
                    </Div>
                </Container>
                <Modal id="obat">
                    <ModalHeader className="close">
                        Data Laporan
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-3">
                                    ID Laporan
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.idLaporan}
                                    </Div>
                                </Div>
                            </Div>
                            {/* <Div className="row">
                                <Div className="col-md-3">
                                    Nama Pembeli
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.nama}
                                    </Div>
                                </Div>
                            </Div> */}
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Transaksi
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.tanggalTransaksi}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Konfirmasi
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.tanggalKonfirmasi !== null ? <> {this.state.tanggalKonfirmasi} </> : <>Menunggu Konfirmasi</>}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Status
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.status}
                                    </Div>
                                </Div>
                            </Div>
                            <br />
                            <Div className="table-responsive">
                                <table className="table table-bordered" style={{ width: "100%" }}>
                                    <thead>
                                        <tr>
                                            <th>Nama Obat</th>
                                            <th>Jenis Obat</th>
                                            <th>Kuantitas</th>
                                            <th>Harga</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.obatList.map((a, index) => {
                                            return (

                                                <tr key={index}>
                                                    <td>{a.namaObat}</td>
                                                    <td>{a.jenis}</td>
                                                    <td>{a.qty}</td>
                                                    <td>{konversiRupiah(a.hargaBarang)}</td>
                                                    <td>{konversiRupiah(a.totalHarga)}</td>
                                                </tr>
                                            )
                                        })
                                        }
                                        <tr>
                                            <td colSpan="4">Biaya Pengiriman</td>
                                            <td>{konversiRupiah(this.state.ongkir)}</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4">Total Pembayaran</td>
                                            <td>{this.state.bayar && konversiRupiah(this.state.bayar)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-secondary" onClick={() => this.closeBtn()}>Tutup</Button>
                    </Div>
                </Modal>
            </>
        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    user: state.AReducer.dataUser


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({


})

export default (connect(mapStateToProps, mapDispatchToProps))(Dashboard);