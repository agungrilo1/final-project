import React, { Component } from 'react';
import Header from '../../../component/header';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import ButtonDelete from '@material-ui/core/Button';
import { connect } from 'react-redux';
import DeleteIcon from '@material-ui/icons/Delete';
import { red } from '@material-ui/core/colors';
import { Button, TitlePage, MenuTitle, Menu, Nav, Active, H5, Container, Label, Div, Input, Main, Tooltip, I } from '../../../component';
import { Footer } from '../../../template';
import swal from 'sweetalert';
import $ from 'jquery';
import Pagination from '@material-ui/lab/Pagination';
import obat from '../../admin/obat';
import konversiRupiah from '../../../component/util';
import ReactTooltip from 'react-tooltip';

const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        padding: '6px 12px',
        border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: '#0063cc',
        borderColor: '#0063cc',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
})(ButtonDelete);

const ColorButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(red[700]),
        backgroundColor: red[700],
        '&:hover': {
            backgroundColor: red[900],
        },
    },
}))(ButtonDelete);


//akhir tabel
class KeranjangMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keranjang: {},
            obatList: [],
            jumlah: [],
            total: 0,
            ongkir: 0,
            bayar: 0,
            qty: [],
            laporan: {},
            statusLaporan: "Sukses",
            checked: false,
        }
    }

    handleChecked = (event) => {
        this.setState({
            [event.target.name]: event.target.checked,
            statusLaporan: event.target.checked ? "Menunggu" : "Sukses"
        }, ()=>{
            console.log("status", this.state);
        })
    }

    getKeranjang = () => {
        let id = this.props.user.idUser;
        console.log("ini id", id);
        fetch(`http://localhost:8080/api/keranjang/?idUser=${encodeURIComponent(id)}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                console.log("json", json);
                this.setState({
                    keranjang: json
                });

                if ($.isEmptyObject(this.state.keranjang) === true) {
                    this.setState({
                        obatList: []
                    });
                } else {
                    this.setState({
                        obatList: this.state.keranjang.obatList
                    });
                }
                let qtyTemp = []
                for (let i = 0; i < this.state.obatList.length; i++) {
                    qtyTemp.push(this.state.obatList[i].qty)
                }
                this.setState({
                    qty: qtyTemp
                })


                let totalTemp = []
                for (let i = 0; i < this.state.obatList.length; i++) {
                    totalTemp.push(this.state.qty[i] * this.state.obatList[i].harga)
                }
                this.setState({
                    jumlah: totalTemp
                })
                let bayarTemp = 0
                for (let i = 0; i < this.state.jumlah.length; i++) {
                    bayarTemp = bayarTemp + this.state.jumlah[i]
                }
                this.setState({
                    total: bayarTemp
                })
                if (this.state.total >= 100000 || this.state.obatList.length > 5 || this.state.total === 0) {
                    this.setState({
                        ongkir: 0
                    })

                } else {
                    this.setState({
                        ongkir: this.state.keranjang.ongkir
                    })
                    // this.setState({
                    //     bayar:this.state.ongkir+this.state.total
                    // })
                }
                this.setState({
                    bayar: this.state.ongkir + this.state.total
                })
                // this.nav();
                if (typeof json.errorMessage !== 'undefined') {
                    swal("Gagal !", json.errorMessage, "error");
                }
            })
            .catch((e) => {
                console.log(e);
            });
    };

    handleChange = (event, value) => {

        this.setState({
            page: value
        })
        console.log("value", value);


    }

    componentDidMount = () => {
        this.getKeranjang();
    }

    hapusItem = (idDetailKeranjang) => {
        swal({
            title: "Apakah kamu yakin?",
            text: "Sekali menghapus, kamu tidak akan bisa memulihkan data tersebut!",
            icon: "warning",
            buttons: {
                cancel: "Batal",
                confirm: "Hapus"
            },
            dangerMode: [true],


        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Item berhasil dihapus!", {
                        icon: "success",
                    });
                    const cari = this.state.obatList.findIndex((idx) => idx.idDetailKeranjang === idDetailKeranjang);
                    const detail = this.state.obatList[cari];
                    const hapusDetailObat = {
                        idDetailKeranjang: idDetailKeranjang
                    }
                    fetch(`http://localhost:8080/api/delete/detail/${idDetailKeranjang}`, {
                        method: "delete",
                        headers: {
                            "Content-Type": "application/json; ; charset=utf-8",
                            "Access-Control-Allow-Headers": "Authorization, Content-Type",
                            "Access-Control-Allow-Origin": "*"
                        },
                        body: JSON.stringify(hapusDetailObat)
                    })
                        .then(resp => {
                            if (!resp.ok) {
                                return resp.json().then(text => {
                                    throw new Error(`${text.errorMessage}`)
                                })
                            } else {
                                return resp.json();
                            }
                        })
                        //menangkap data dari fetch

                        .then(json => {
                            swal({
                                title: `${json.errorMessage}`,
                                text: "Berhasil menghapus data",
                                icon: "success"
                            })

                            this.getKeranjang();

                        })
                        .catch((e) => {
                            swal({
                                title: "perhatian",
                                text: `${e}`,
                                icon: "error",
                            });

                        });
                }
            });


    }

    hapusSemuaItem = () => {
        swal({
            title: "Apakah kamu yakin?",
            text: "Sekali menghapus, kamu tidak akan bisa memulihkan data tersebut!",
            icon: "warning",
            buttons: {
                cancel: "Batal",
                confirm: "Hapus"
            },
            dangerMode: [true],


        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Keranjang berhasil di hapus", {
                        icon: "success",
                    });
                    console.log("id", this.state.keranjang.idKeranjang);
                    let id = this.state.keranjang.idKeranjang
                    fetch(`http://localhost:8080/api/delete/all/detail/${id}`, {
                        method: "delete",
                        headers: {
                            "Content-Type": "application/json; ; charset=utf-8",
                            "Access-Control-Allow-Headers": "Authorization, Content-Type",
                            "Access-Control-Allow-Origin": "*"
                        },
                    })
                        .then(resp => {
                            if (!resp.ok) {
                                return resp.json().then(text => {
                                    throw new Error(`${text.errorMessage}`)
                                })
                            } else {
                                return resp.json();
                            }
                        })
                        //menangkap data dari fetch

                        .then(json => {
                            this.getKeranjang()
                            swal({
                                title: `${json.errorMessage}`,
                                text: "Berhasil menghapus produk",
                                icon: "success"
                            })
                                .then((value) => {
                                    // this.clearField();
                                    //  $("#form-obat .close").click();
                                    // this.get();
                                    // this.createDetail();
                                    this.getKeranjang()
                                    this.setState({
                                        total: 0
                                    });
                                })

                        })
                        .catch((e) => {
                            swal({
                                title: "perhatian",
                                text: `${e}`,
                                icon: "error",
                            });

                        });
                }
            });


    }

    clearCart = () => {
        let id = this.state.keranjang.idKeranjang
        fetch(`http://localhost:8080/api/delete/all/detail/${id}`, {
            method: "delete",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            },
        })
            .then(resp => {
                if (!resp.ok) {
                    return resp.json().then(text => {
                        throw new Error(`${text.errorMessage}`)
                    })
                } else { return resp.json(); }
            })
            //menangkap data dari fetch

            .then(json => {
                this.getKeranjang()
                this.setState({
                    total: 0
                });
            })
            .catch((e) => {
                console.log(e);

            });

    }

    setValue = (value, idDetailKeranjang) => {
        fetch(`http://localhost:8080/api/update-qty/?id=` + idDetailKeranjang + `&value=` + value + ``, {
            method: "put",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            },
        })
            .then((resp) => resp.json())
            //menangkap data dari fetch
            .then(json => {
            })
            .catch(() => {
            });
        this.getKeranjang();
        // console.log(":::::::::", this.state.obatList);
    }

    beliProduk = () => {
        swal({
            title: "Apakah kamu yakin?",
            text: "Proses pembelian akan dilakukan jika disetujui",
            icon: "warning",
            buttons: {
                cancel: "Batal",
                confirm: "Beli"
            },

        })
            .then((willDelete) => {
                if (willDelete) {
                    let id = this.props.user.idUser;
                    let arrObatList = []
                    for (let i = 0; i < this.state.obatList.length; i++) {
                        let isiObat = {
                            idObat: this.state.obatList[i].idObat,
                            qty: this.state.obatList[i].qty,
                            hargaBarang: this.state.obatList[i].harga,
                            totalHarga: this.state.jumlah[i],
                            statusLaporan: this.state.statusLaporan

                        }
                        arrObatList.push(isiObat)
                        console.log("isi obat", isiObat);
                        // arr.push(this.state.laporan);
                    }
                    const beli = {
                        idUser: id,
                        biaya: this.state.total,
                        ongkir: this.state.ongkir,
                        total: this.state.bayar,
                        statusLaporan: this.state.statusLaporan,
                        obatList: arrObatList
                    }
                    console.log("beli", beli);
                    // this.setState({
                    //     laporan:arr,
                    // })
                    console.log("ini id user untuk laporan", id);
                    fetch(`http://localhost:8080/api/create-laporan/?idUser=` + id + ``, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json; ; charset=utf-8",
                            "Access-Control-Allow-Headers": "Authorization, Content-Type",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(beli)
                    })
                        .then(resp => {
                            console.log(resp);
                            if (!resp.ok) {
                                return resp.json().then(text => {
                                    throw new Error(`${text.errorMessage}`)
                                })
                            } else {
                                return resp.json();

                            }
                        })
                        .then(json => {
                            console.log(this.state.checked);
                            if (this.state.checked === true) {
                                swal("Pembelian sedang diproses, Silahkan ditunggu", {
                                    icon: "success",
                                })
                                .then((value) => {
                                    $("#obat .close").click();
                                    this.clearCart()
                                    this.getKeranjang()

                                })
                            } else {
                                swal("Pembelian berhasil", {
                                    icon: "success",
                                })
                                .then((value) => {
                                    $("#obat .close").click();
                                    this.clearCart()
                                    this.getKeranjang()
                                })
                            }
                            // swal({
                            //     title: `${json.errorMessage}`,
                            //     icon: "success",
                            // })

                            //     .then((value) => {
                            //         $("#obat .close").click();
                            //         this.clearCart()
                            //         this.getKeranjang()

                            //     })
                        })
                        .catch((e) => {
                            swal({
                                title: "perhatian",
                                text: `${e}`,
                                icon: "error",
                            });

                        });

                }

            });



    }


    nav = () => {
        if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Admin") {
            this.props.history.push("/dashboard-admin")
        }
    }

    render() {

        console.log("obat list", this.state.obatList);
        console.log("ongkir", this.state.ongkir);
        this.nav();
        const classes = () => this.props.useStyles();
        const classesButton = () => this.props.useStylesButton();
        return (
            <>
                <Container>


                    <Header />
                    <div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                        </MenuTitle>
                            <Link to="/dashboard">
                                <Menu icon="fas fa-tachometer-alt">
                                    Beranda
                            </Menu>
                            </Link>
                            <Link to="/obat">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                    Obat
                            </Menu>
                            </Link>
                            <Link to="/keranjang-member">
                                <Active active="active">
                                    <Menu icon="fas fa-shopping-cart">
                                        Keranjang
                            </Menu>
                                </Active>
                            </Link>
                            <Link to="/profil">
                                <Menu icon="fas fa-user">
                                    Profil
                            </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Main >
                                <Div className="container-fluid">
                                    <div className="title-table">
                                        <TitlePage icon="fas fa-shopping-cart" >
                                            {/* KERANJANG */}
                                        </TitlePage>
                                        <div className="title">

                                            <div style={{ float: "left" }}>
                                                <h4>
                                                    Keranjang
                                                </h4>
                                            </div>
                                            {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ?
                                                ""
                                                :
                                                <div style={{ float: "right" }}>
                                                    <Button
                                                        
                                                        className="btn btn-danger"
                                                        // variant="contained"
                                                        // color="primary"
                                                        id="delete"
                                                        // className={classes.button}
                                                        // startIcon={<DeleteIcon />}
                                                        onClick={() => { this.hapusSemuaItem(this.state.keranjang.idKeranjang) }}
                                                    >
                                                        <i className="fas fa-trash-alt mr-1"/>

                                                        Batal Semua
                                            </Button>
                                                </div>
                                            }

                                        </div>
                                    </div>

                                    <table className="table table table-striped">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>No</th>
                                                <th scope="col">Nama Obat</th>
                                                <th scope="col">Harga</th>
                                                <th width="140">Kwantitas</th>
                                                <th scope="col">Sub Total</th>
                                                {/* <th scope="col">Tanggal Registrasi</th> */}
                                                <th style={{ textAlign: "center" }} scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ?
                                                <tr>
                                                    <td
                                                        colSpan="7"
                                                        align="center"
                                                    >Keranjang kosong</td>
                                                </tr>
                                                : <>
                                                    {
                                                        this.state.obatList !== null &&
                                                        this.state.obatList.map((a, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{a.namaObat}</td>
                                                                    <td>{konversiRupiah(a.harga)}</td>
                                                                    <td><Input
                                                                        type="number"
                                                                        min="1"
                                                                        className="form-control col-7"
                                                                        name="qty"
                                                                        value={this.state.qty[index]}
                                                                        onChange={event => { this.setValue(event.target.value, a.idDetailKeranjang) }}
                                                                        max={a.stok}
                                                                    />
                                                                    </td>
                                                                    <td>{konversiRupiah(this.state.jumlah[index])}</td>
                                                                    {/* <td>{a.jumlahBayar}</td> */}
                                                                    {/* <td>{a.ongkir}</td> */}
                                                                    <td
                                                                    // className="td"
                                                                    >
                                                                        <div>
                                                                            <div>
                                                                                <center>
                                                                                    {this.state.obatList.length === 1 ?
                                                                                    <>
                                                                                    <Tooltip isi="Batal">
                                                                                        <Button
                                                                                            onClick={() => { this.hapusSemuaItem(this.state.keranjang.idKeranjang) }}
                                                                                            className="btn btn-danger"
                                                                                        >

                                                                                            <I className="fa fa-trash-alt"/>
                                                                                        </Button>
                                                                                        </Tooltip>
                                                                                        <ReactTooltip/>
                                                                                        </>
                                                                                        :
                                                                                        <>
                                                                                        <Tooltip isi="Batal">
                                                                                        <Button
                                                                                            onClick={() => { this.hapusItem(a.idDetailKeranjang) }}
                                                                                            className="btn btn-danger"
                                                                                        ><I className="fa fa-trash-alt"/></Button>
                                                                                        </Tooltip>
                                                                                        <ReactTooltip/>
                                                                                        </>
                                                                                    }

                                                                                </center>
                                                                            </div>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </>
                                            }
                                        </tbody>
                                    </table>
                                    {(this.state.keranjang.idKeranjang) === null || this.state.total === 0 ?
                                        <>
                                            <div>
                                                <center>
                                                    <h3>Apakah anda ingin menambahkan Produk?</h3>
                                                    <button className="btn btn-primary"
                                                        onClick={() => { this.props.history.push("/obat") }}>
                                                        Tambah
                                                </button>
                                                </center>
                                            </div>
                                        </>
                                        :
                                        <Div>
                                            <Div style={{ float: "right" }}>
                                                <Div style={{ paddingRight: "10vh" }}>Masukkan Resep</Div>
                                                <br />
                                                <Label className="switch">
                                                    <Input
                                                        type="checkbox"
                                                        cheked={this.state.checked}
                                                        name="checked"
                                                        onChange={this.handleChecked} />
                                                    <span className="slider round" />
                                                </Label>
                                            </Div>
                                            <Div className="row">
                                                <Div className="col-md-2">
                                                    Total
                                            </Div>
                                                <Div className="col-md-9 row">
                                                    <Div className="col-md-12">
                                                        : {konversiRupiah(this.state.total)}
                                                    </Div>
                                                </Div>
                                            </Div>
                                            <Div className="row">
                                                <Div className="col-md-2">
                                                    Biaya Pengiriman
                                            </Div>
                                                <Div className="col-md-9 row">
                                                    <Div className="col-md-12">
                                                        : {konversiRupiah(this.state.ongkir)}
                                                    </Div>
                                                </Div>
                                            </Div>
                                            <Div className="row">
                                                <Div className="col-md-2">
                                                    Total Bayar
                                            </Div>
                                                <Div className="col-md-9 row">
                                                    <Div className="col-md-12">
                                                        : {konversiRupiah(this.state.bayar)}
                                                    </Div>
                                                </Div>
                                            </Div>
                                            <Div className="form-group" style={{ float: "left" }}>
                                                <br />
                                                <Div id="resep" style={{ float: "left" }}>
                                                    <Button style={{width:"40vh"}} className="btn btn-primary"
                                                        onClick={() => { this.beliProduk() }}
                                                    >Beli</Button>
                                                </Div>
                                            </Div>
                                        </Div>

                                    }
                                </Div>
                            </Main>
                            {/* <Footer>
                                Copyright © Rilo 2021
                        </Footer> */}
                        </Div>
                    </div>

                </Container>
            </>
        );
    }
}
//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    user: state.AReducer.dataUser


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({


})

export default (connect(mapStateToProps, mapDispatchToProps))(KeranjangMember);