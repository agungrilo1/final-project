import React, { Component } from 'react';
import { Label, Input, Button, Background, Layout, Form, FormGroup, Div, I } from '../../component';
import login from '../../asset/apotek2.png';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { FaEye, FaEyeSlash } from 'react-icons/fa';


class ChangePass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pengguna: {},
            namaPengguna: "",
            kataSandiLama: "",
            kataSandi: "",
            konfirmasiKataSandi: "",
            passType: true,
            passType2: true,
            passType3: true,
        }
    }

    setValue = el => {
        this.setState({
            [el.target.name]: el.target.value
        })

    }

    passClick = () => {
        console.log("pass");
        // const passTypeTemp = this.state.passType
        this.setState({
            passType: !this.state.passType,

        })

    }

    passClick2 = () => {
        this.setState({
            passType2: !this.state.passType2
        })
    }

    passClick3 = () => {
        this.setState({

            passType3: !this.state.passType3
        })
    }

    doLogout = () => {
        swal({
            title: "Apakah anda yakin ingin keluar?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.logoutEvent();
                    swal("Anda berhasil keluar", {
                        icon: "success",
                        title: "Terima Kasih"
                    });
                }
            });
    }

    doChangePass = () => {
        let object = this.state;
        let regKataSandi = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*[&%$]).{6,8}$/;
        console.log("pas :", regKataSandi.test(object.kataSandi));
        const regpas = regKataSandi.test(object.kataSandi);
        //true lanjut
        //false block
        if (object.kataSandiLama === "" || object.kataSandi === "" || object.konfirmasiKataSandi === "") {
            swal({
                title: "Gagal !!",
                text: "Harap diisi tidak boleh kosong",
                icon: "warning"
            })
        } else if (object.kataSandi === this.props.user.password) {
            swal({
                title: "Gagal !!",
                text: "Tidak boleh menggunakan kata sandi lama",
                icon: "warning"
            })
        } else if (regpas === false) {
            swal({
                title: "Gagal !!",
                text: "Kata sandi harus terdiri dari 6 sampai 8 alfanumerik dan minimal 1 huruf kapital",
                icon: "warning"
            })
        } else if (object.kataSandiLama !== this.props.user.password) {
            swal({
                title: "Gagal !!",
                text: "Kata sandi lama tidak sesuai",
                icon: "warning"
            })
        } else if (object.kataSandi !== object.konfirmasiKataSandi) {
            swal({
                title: "Gagal !!",
                text: "konfirmasi kata sandi tidak sesuai dengan kata sandi baru",
                icon: "warning"
            })
        } else {
            swal({
                title: "Apakah anda yakin ingin mengganti kata sandi",
                text: "Setelah anda mengubah kata sandi anda akan diarahkan ke halaman awal masuk",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Kata sandi berhasil diubah", {
                            icon: "success",
                        });
                        const changePass = {
                            idUser: this.props.user.idUser,
                            password: this.state.kataSandi
                        }
                        fetch(`http://localhost:8080/api/change-password/`, {
                            method: "put",
                            headers: {
                                "Content-Type": "application/json; ; charset=utf-8",
                                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                                "Access-Control-Allow-Origin": "*"
                            },
                            body: JSON.stringify(changePass)
                        })
                            .then((response) => response.json())
                            .then((json) => {
                                console.log("ini json", json);
                                console.log("ini json", json.errorMessage);
                                if (json.errorMessage === "undefined") {
                                    swal("Gagal !", json.errorMessage, "error");

                                } else {
                                    swal({
                                        icon: "success",
                                        title: "Berhasil"
                                    })
                                        .then((value) => {
                                            this.props.logoutEvent();
                                            this.props.history.push("/");
                                        })
                                }
                            })
                            .catch((e) => {
                                console.log(e);
                                swal("Gagal !", "Gagal mengambil data", "error");
                            });
                    }
                });
        }
    }

    doCancel=()=>{
        swal({
            title: "Apakah anda yakin?",
            text: "Anda akan membatalkan proses ganti kata sandi",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal({
                        icon: "success",
                        title: "Terima kasih"
                    })
                        .then((value) => {
                            this.props.logoutEvent();
                            this.props.history.push("/");
                        })
                }
            })
    }



    nav = () => {
       if (
            this.props.checkLogin === true &&
            this.props.user.password !== "User123"
        ) {
            this.props.history.push("/dashboard");
        } else if (this.props.checkLogin === false) {
            this.props.history.push("/")
        }
    }


    render() {
        this.nav();
        console.log(this.state.passType);
        return (
            <Background>
                <Layout>
                    <Form>
                        <FormGroup className="form-group">
                            <Label className="small mb-1">Kata Sandi Lama</Label>
                            <Input
                                className="form-control py-4"
                                type={this.state.passType ? "password" : "text"}
                                name="kataSandiLama"
                                id="password-field"
                                placeholder="Masukkan Kata Sandi"
                                onChange={this.setValue}
                            />
                            <Button
                                className="show-pasword "
                                style={{
                                    border: "none",
                                    textDecoration: "none",
                                    backgroundColor: "Transparent",
                                    cursor: "pointer",
                                    overflow: "hidden",
                                    outline: "none",
                                    float: "right",
                                    marginLeft: -25,
                                    marginTop: -35,
                                    position: "relative",
                                    zIndex: 2
                                }} onClick={() => { this.passClick() }} >
                            
                                {this.state.passType ? <FaEyeSlash /> : <FaEye />}
                            </Button>
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label className="small mb-1">Kata Sandi Baru</Label>
                            <Input
                                className="form-control py-4"
                                type={this.state.passType2 ? "password" : "text"}
                                name="kataSandi"
                                id="password-field"
                                placeholder="Masukkan Kata Sandi"
                                onChange={this.setValue}
                            />
                            <Button
                                className="show-pasword "
                                style={{
                                    border: "none",
                                    textDecoration: "none",
                                    backgroundColor: "Transparent",
                                    backgroundRepeat: "no-repeat",
                                    border: "none",
                                    cursor: "pointer",
                                    overflow: "hidden",
                                    outline: "none",
                                    float: "right",
                                    marginLeft: -25,
                                    marginTop: -35,
                                    position: "relative",
                                    zIndex: 2
                                }} onClick={() => { this.passClick2() }} >
                                
                                {this.state.passType2 ? <FaEyeSlash /> : <FaEye />}
                            </Button>
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label className="small mb-1">Konfirmasi Kata Sandi Baru</Label>
                            <Input
                                className="form-control py-4"
                                type={this.state.passType3 ? "password" : "text"}
                                name="konfirmasiKataSandi"
                                id="password-field"
                                placeholder="Masukkan Kata Sandi"
                                onChange={this.setValue}
                            />
                            <Button
                                className="show-pasword "
                                style={{
                                    border: "none",
                                    textDecoration: "none",
                                    backgroundColor: "Transparent",
                                    cursor: "pointer",
                                    overflow: "hidden",
                                    outline: "none",
                                    float: "right",
                                    marginLeft: -25,
                                    marginTop: -35,
                                    position: "relative",
                                    zIndex: 2
                                }} onClick={() => { this.passClick3() }} >
                                
                                {this.state.passType3 ? <FaEyeSlash /> : <FaEye />}
                            </Button>
                        </FormGroup>
                        <FormGroup className="d-flex flex-row">
                            <Button id="masuk"  className="btn btn-danger" onClick={() => this.doCancel()}>Batal</Button>
                            <Button id="masuk" style={{marginLeft:"43vh"}} className="btn btn-primary" onClick={() => this.doChangePass()}>Simpan</Button>
                            
                        </FormGroup>
                    </Form>
                </Layout>
            </Background>
        );
    }
}


//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser

})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({
    submitLogin: (data) => dispatch({ type: "LOGIN_SUCCESS", payload: data }),
    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })

})

export default (connect(mapStateToProps, mapDispatchToProps))(ChangePass);