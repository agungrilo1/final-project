import React, { Component } from 'react';
import { Menu, MenuTitle, Nav, Active, Header, Div, Container, TitlePage, Input, Button,I, Modal, ModalHeader, FormRow, Label, TextArea, Tooltip } from "../../../component"
import Footer from '../../../template/footer';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import swal from 'sweetalert';
import Pagination from '@material-ui/lab/Pagination';
import ReactToPrint from 'react-to-print';
import konversiRupiah from '../../../component/util';
import ReactTooltip from 'react-tooltip';


class Resep extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            limit: 5,
            page: 1,
            count: 0,
            total: "",
            resep: [],
            cetakResep:[],
            obatList:[],
            idLaporan: "",
            nama: "",
            tanggalTransaksi: "",
            total: 0,
            ongkir: 0,
            harga: 0,
            status: "",
            statusLaporan:"",
            // statusGet : false,
            kondisiCari:false,
            cari1: "",
            cari2:"",
            cari3:"",
            pageNow : 1
         }
    }

    getResep = (page, limit) => {
        fetch('http://localhost:8080/api/recipe/?limit=' + limit + '&page=' + page + '', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    resep: json,
                    page : page
                });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
               
            });
    }

    getCountResep = () => {
        fetch('http://localhost:8080/api/recipe-count/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    count: Math.ceil(Number(json) / this.state.limit)
                });
            })
            .catch((e) => {
                console.log(e);
                console.log("tes", this.state.obat);
                alert("Failed fetching data!!");
            });
    }

    handleChange = (event, value) => {
        this.setState({
            page: value
        })
        console.log("value", value);
        if(this.state.cari1 !== "" || this.state.cari2 !== "" || this.state.cari3 !== ""){
            this.searchPagination(value, this.state.limit);
        }else{
            this.getResep(value, 5);
            this.getCountResep();
        }
    }

    searchPagination = (page, limit) => {
        if(this.state.cari1 === "" && this.state.cari2 === "" && this.state.cari3 === ""){
            swal({
                title: "Perhatian",
                icon: "warning",
                text: "Harap masukkan kata pencarian"
            })
        }else{
            this.setState({
                kondisiCari : true,
                page : page
            })
            let keyword1 = this.state.cari1
            let keyword2 = this.state.cari2
            let keyword3 = this.state.cari3
            console.log("count juga",this.state.count);
            if (page){}else { page = 1 }
        Promise.all([
            fetch(`http://localhost:8080/api/search/recipe/?keyword1=${keyword1}&keyword2=${keyword2}&keyword3=${keyword3}&limit=${this.state.limit}&page=${page}`),
            fetch('http://localhost:8080/api/search-counter/recipe/?keyword1='+keyword1+'&keyword2='+keyword2+'&keyword3=' + keyword3),
        ])
            .then(([response, 
                response2
            ]) => Promise.all([response.json(), 
                response2.json()
            ]))
            .then(([json
                , json2
            ]) => {
                
                this.setState({
                    resep : json,
                    count: Math.ceil(Number(json2.countSearch) / this.state.limit),
                    page:page
                })
                
            })
        }
        

    }

    setValue = el => {
        if(el.target.name === "cari1" && this.state.cari2 === "" && this.state.cari3 === "" && el.target.value === "" ){
            this.getResep(this.state.pageNow, this.state.limit);
            this.getCountResep();
        }else if(el.target.name === "cari2" && this.state.cari1 === "" && this.state.cari3 === "" && el.target.value === "" ){
            this.getResep(this.state.pageNow, this.state.limit);
            this.getCountResep();
        }else if(el.target.name === "cari3" && this.state.cari1 === "" && this.state.cari2 === "" && el.target.value === "" ){
            this.getResep(this.state.pageNow, this.state.limit);
            this.getCountResep();
        }
        this.setState({
            [el.target.name]: el.target.value,
        })
    }

    confirmRecipe=()=>{
        swal({
            title: "Apa kamu yakin?",
            text: "Proses tidak dapat diulangi kembali setelah menyetujinya",
            icon: "warning",
            buttons: {
                cancel: "Batal",
                confirm: "Konfirmasi"
            }
          })
          .then((willConfirm) => {
            if (willConfirm) {
                swal("Resep berhasil diberi tindakan", {
                    icon: "success",
                  })
              this.setState({
                statusLaporan:"Sukses"
            })
            this.updateStatus();
            
            }
          });
    }

    rejectRecipe=()=>{
        swal({
            title: "Apa kamu yakin?",
            text: "Proses tidak dapat diulangi kembali setelah menyetujinya",
            icon: "warning",
            buttons: {
                cancel: "Batal",
                confirm: "Konfirmasi"
            },
          })
          .then((willReject) => {
            if (willReject) {
              swal("Resep berhasil diberi tindakan", {
                icon: "success",
              })
              .then((value) => {
                $("#resep .close").click();
                this.getResep();

            })
              
              this.setState({
                statusLaporan:"Gagal"
            })
            
            this.updateStatus();
            
            } 
          });
    }


    updateStatus=()=>{
        console.log("idLaporan",this.state.idLaporan);
        console.log("statusLaporan",this.state.statusLaporan);
        const changeStatus = {
            statusLaporan : this.state.statusLaporan,
            idLaporan: this.state.idLaporan
        }
        fetch(`http://localhost:8080/api/recipe-confirm/`,{
            method: "put",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify(changeStatus)
        })
        .then(resp => {

            if (!resp.ok) {
                return resp.json().then(text => {
                    throw new Error(`${text.errorMessage}`)
                })
            } else {
                return resp.json();
            }
        })
        //menangkap data dari fetch

        .then(json => {

            // this.clearField();
            swal({
                title: `${json.errorMessage}`,
                icon: "success",
            })
                .then((value) => {
                    $("#resep .close").click();
                    this.getResep(this.state.pageNow,this.state.limit);
                    

                })
                
        })
        .catch((e) => {
            swal({
                title: "perhatian",
                text: `${e}`,
                icon: "error",
            });

        });
    }

    detailResepValue = (idLaporan) => {
        console.log(idLaporan);
        const cari = this.state.resep.findIndex((idx) => idx.idLaporan === idLaporan);
        const detail = this.state.resep[cari];
        console.log(detail);
        this.setState({
            idLaporan: detail.idLaporan,
            nama: detail.name,
            tanggalTransaksi: detail.tglTransaksi,
            tanggalKonfirmasi: detail.tglKonfirmasi,
            status: detail.statusLaporan,
            bayar: detail.total,
            ongkir: detail.ongkir,
            obatList: detail.obatList
        });
    }

    componentDidMount = () => {
        this.getResep(this.state.page, this.state.limit);
        this.getCountResep();
        // this.getReportPrint();
        // this.interval = setInterval(() => this.setState({ time: Date.now() }), 10);
    }
    closeBtn = () => {
        $("#resep .close").click();
    }

    nav = () => {
        if (this.props.checkLogin === false ) {
            this.props.history.push("/");
        } else if(this.props.checkLogin === true &&
            this.props.user.role === "Member") {
            this.props.history.push("/dashboard")
        }
    }

    render() {
        this.nav();
        // console.log("id Laporan", this.state.resep);
        const {page, limit}=this.state
        console.log("resep",this.state.resep.length);
        return ( 
            <>
                <Container>
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                            </MenuTitle>
                            <Link to="/dashboard-admin">
                                <Menu icon="fas fa-tachometer-alt">
                                Beranda
                            </Menu>
                            </Link>
                            <Link to="/obat/admin">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                        Obat
                                </Menu>
                            </Link>
                            <Link to="/pengguna">
                                <Menu icon="fas fa-user">
                                    Pengguna
                                </Menu>
                            </Link>
                            <Link to="/resep">
                            <Active active="active">
                                <Menu icon="fas fa-file-medical">
                                    Resep
                                </Menu>
                            </Active>
                            </Link>
                            <Link to="/laporan">
                                <Menu icon="fas fa-book">
                                    Laporan
                                </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Div className="container-table">
                                <Div className="judul-pengguna">
                                    <TitlePage icon="fas fa-user" >
                                        Resep Pembelian
                                    </TitlePage>
                                </Div>
                                
                                       <Div className="row">
                                            <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                                <Input
                                                    type="search"
                                                    name="cari1"
                                                    className="form-control"
                                                    value={this.state.cari1}
                                                    placeholder="Cari Nama pembeli"
                                                    onChange={this.setValue}
                                                />
                                            </Div>
                                            <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                                <Input
                                                    type="date"
                                                    name="cari2"
                                                    className="form-control"
                                                    value={this.state.cari2}
                                                    // placeholder="Cari Nama pembeli, Obat, Tanggal Pembelian"
                                                    onChange={this.setValue}
                                                />
                                            </Div>
                                            <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                                <Input
                                                    type="search"
                                                    name="cari3"
                                                    value={this.state.cari3}
                                                    className="form-control"
                                                    placeholder="Cari Obat"
                                                    onChange={this.setValue}
                                                />
                                            </Div>
                                            <Div className="col-1">
                                                <Button
                                                    type="button"
                                                    className="btn btn-primary"
                                                    onClick={() => { this.searchPagination() }}
                                                >
                                                    <I className="fas fa-search" />
                                                </Button>
                                            </Div>
                                    </Div>
    
                                    <Div className="tabel">
                                        <table className="table table-bordered table-striped">
                                            <thead className="thead-dark">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Nama Pembeli</th>
                                                    <th scope="col">Tanggal Transaksi</th> 
                                                    <th hidden="hidden" scope="col">Nama Obat</th>
                                                    <th scope="col">Jumlah Barang</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Harga</th>
                                                    <th style={{ textAlign: "center" }} scope="col">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {this.state.resep.length === 0 ?
                                            <tr>
                                                <td
                                                    colSpan="7"
                                                    align="center"
                                                >Data kosong</td>
                                            </tr>
                                            :
                                            <>
                                                 {this.state.resep.map((a, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{(((page*limit)-limit + 1)+index)}</td>
                                                            <td>{a.name}</td>
                                                            <td>{a.tglTransaksi}</td>
                                                            <td hidden="hidden">
                                                                {/* {a.obatList.map((b, index) => {
                                                                    return (

                                                                        <span key={index}>
                                                                            <p>
                                                                                - {b.namaObat}
                                                                            </p>
                                                                        </span>
                                                                    )
                                                                })
                                                                } */}
                                                            </td>
                                                            <td >{a.obatList.length} Jenis</td>
                                                            <td ><span className="badge badge-secondary">{a.statusLaporan}</span></td>
                                                            <td>{konversiRupiah(a.total)}</td>
                                                            <td>
                                                                <center>
                                                                <Tooltip isi="Detail">

                                                                    <Button
                                                                        className="btn btn-secondary"
                                                                        dataToogle="modal"
                                                                        dataTarget="#resep"
                                                                        data-target="#exampleModal"
                                                                        onClick={() => { this.detailResepValue(a.idLaporan) }}
                                                                        ><I className="fas fa-info-circle"></I></Button>
                                                                        </Tooltip>
                                                                    <ReactTooltip />
                                                                </center>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                                }
                                            </>
                     
                                            }
                                            </tbody>
                                        </table>
                                        {this.state.resep.length !== 0  &&
                                    <Div className="pagination" >
                                        {/* <Typography>Page: {page}</Typography> */}
                                        <Pagination count={this.state.count} page={this.state.page}
                                            onChange={this.handleChange}
                                        />
                                    </Div>
                                    }
                                </Div>
                            </Div>
                            <Footer>
                                Copyright © Rilo 2021
                            </Footer>
                        </Div>
                    </Div>
                </Container>
                <Modal id="resep">
                    <ModalHeader className="close">
                        Data Laporan
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-3">
                                    ID Laporan
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.idLaporan}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Nama Pembeli
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.nama}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Transaksi
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.tanggalTransaksi}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Status
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.status}
                                    </Div>
                                </Div>
                            </Div>
                            <br />
                            <Div className="table-responsive">
                                <table className="table table-bordered" style={{ width: "100%" }}>
                                    <thead>
                                        <tr>
                                            <th>Nama Obat</th>
                                            <th>Jenis Obat</th>
                                            <th>Kuantitas</th>
                                            <th>Harga</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.obatList.map((a, index) => {
                                            return (

                                                <tr key={index}>
                                                    <td>{a.namaObat}</td>
                                                    <td>{a.jenis}</td>
                                                    <td>{a.qty}</td>
                                                    <td>{konversiRupiah(a.hargaBarang)}</td>
                                                    <td>{konversiRupiah(a.totalHarga)}</td>
                                                </tr>
                                            )
                                        })
                                        }
                                        <tr>
                                            <td colSpan="4">Biaya Pengiriman</td>
                                            <td>{konversiRupiah(this.state.ongkir)}</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4">Total Pembayaran</td>
                                            <td>{this.state.bayar && konversiRupiah(this.state.bayar)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                            <Button className="btn btn-primary" onClick={()=>{this.confirmRecipe()}}><I className="fa fa-check"/> Terima</Button>
                            <Button className="btn btn-danger"  onClick={()=>{this.rejectRecipe()}}><I className="fa fa-times"/> Tolak</Button>
                        <Button type="button" className="btn btn-secondary" onClick={() => this.closeBtn()}>Tutup</Button>
                    </Div>
                </Modal>
            </>
         );
    }
}
 
const mapStateToProps = state => ({
    login: state.AReducer.dataUser,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser,
    checkLogin: state.AReducer.isLogin


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({

    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })
})

export default (connect(mapStateToProps, mapDispatchToProps))(Resep);
