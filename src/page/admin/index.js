import User from './user';
import DashboardAdmin from './dashboard';
import ObatAdmin from './obat';
import Laporan from './laporan';
import Resep from './resep';

export { User, DashboardAdmin, ObatAdmin, Laporan, Resep}