import React, { Component } from 'react';
import { Menu, Container, Div, Active, Header, Nav, Tooltip, TitlePage, Input, I, Button, Label, Modal, ModalHeader, FormRow, TextArea } from "../../../component";
import { Link } from 'react-router-dom';
import Footer from '../../../template/footer';
import swal from 'sweetalert';
import $ from 'jquery';
import { connect } from 'react-redux';
import Pagination from '@material-ui/lab/Pagination';
import ReactTooltip from 'react-tooltip';



class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            profilPengguna: {},
            id: "",
            nama: "",
            alamat: "",
            namaPengguna: "",
            hp: "",
            kataSandi: "",
            tanggalRegistrasi: "",
            konfirmasiKataSandi: "",
            idubah: true,
            disabled: false,
            userEdit: {},
            button: "Simpan",
            //pagination
            limit: 5,
            page: 1,
            count: 0,
            total: "",
            cari: "",
            visible: "",
            type: "",
            kondisiCari: false,
            pageNow: 1,
            api: "http://localhost:8080/api/"


        }
    }

    setValue = el => {
        if (el.target.name === "cari" && el.target.value === "") {
            this.getUsersPaging(this.state.pageNow, this.state.limit);
            this.getCountUser();
            // this.setState({
            //     page: 1
            // })
        }
        this.setState({
            [el.target.name]: el.target.value,
        })

    }

    clearField = () => {
        this.setState({
            id: "",
            nama: "",
            alamat: "",
            namaPengguna: "",
            hp: "",
            email: ""
        })
    }

    //pagination

    getCountUser = () => {
        fetch('' + this.state.api + 'count/users/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    count: Math.ceil(Number(json) / this.state.limit)
                });
                console.log("count user", this.state.count);
                console.log("count user", Number(json));
            })
            .catch((e) => {
                alert(e);
            });
    }



    getUsersPaging = (page, limit) => {
        // if (page !== undefined) { page = this.state.page }
        // else { }
        if(page){}else{page=this.state.page}
        if(limit){}else{limit=this.state.limit}
        fetch('' + this.state.api + 'user-pagination/?page=' + page + '&limit=' + limit + '', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ users: json,
                page:page });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    handleChange = (event, value) => {

        this.setState({
            page: value
        })
        if (this.state.cari !== "") {
            this.searchPagination(value, this.state.limit)
        } else {
            this.getUsersPaging(value, 5);
            this.getCountUser();
        }

    }


    //pagination

    searchPagination = (page, limit) => {
        if (this.state.cari === "") {
            swal({
                title: "Perhatian",
                icon: "warning",
                text: "Harap masukkan kata pencarian"
            })
        } else {
            this.setState({
                kondisiCari: true,
                page: page
            })
            let keyword = this.state.cari
            if (page) { } else { page = 1 }
            Promise.all([
                fetch(`http://localhost:8080/api/search/?keyword=${keyword}&page=${page}&limit=${this.state.limit}`),
                fetch('http://localhost:8080/api/search-counter/?keyword=' + keyword),
            ])
                .then(([response, response2]) => Promise.all([response.json(), response2.json()]))
                .then(([json, json2]) => {
                    console.log(Math.ceil(Number(json2) / this.state.limit));
                    console.log(parseInt(json2));
                    console.log(json2);
                    this.setState({
                        users: json, 
                        count: Math.ceil(Number(json2.countSearch) / this.state.limit),
                        page: page
                    })
                    console.log(json);
                    console.log("ini count", this.state.count);
                    console.log("count", this.state.count);
                })
        }

    }

    getUser = () => {
        console.log("profil", this.props.users);
        fetch('http://localhost:8080/api/users/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ users: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }


    adminValue = (idUser) => {
        const cari = this.state.users.findIndex((idx) => idx.idUser === idUser);
        const detail = this.state.users[cari];
        console.log(detail);
        this.setState({
            id: detail.idUser,
            namaPengguna: detail.username,
            nama: detail.name,
            alamat: detail.address,
            hp: detail.phone,
            email: detail.email,
            disabled: true,
            button: "Sunting"
        });

    }


    resetPassword = (idUser) => {
        const cari = this.state.users.findIndex((idx) => idx.idUser === idUser);
        const detail = this.state.users[cari];
        this.setState({
            id: detail.idUser,
        });
        swal({
            title: "Apakah kamu yakin ?",
            text: "Kata sandi user akan di kembalikan ke pengaturan awal",
            icon: "warning",
            buttons: [true, "Yakin"],
            confirm: [true, "Batal"],
        })
            .then((willReset) => {
                if (willReset) {
                    swal("Poof! Your imaginary file has been deleted!", {
                        icon: "success",
                    })
                    const reset = {
                        idUser: this.state.id,
                    }
                    fetch(`http://localhost:8080/api/reset/`, {
                        method: "put",
                        headers: {
                            "Content-Type": "application/json; ; charset=utf-8",
                            "Access-Control-Allow-Headers": "Authorization, Content-Type",
                            "Access-Control-Allow-Origin": "*"
                        },
                        body: JSON.stringify(reset)
                    })
                        .then(resp => {

                            if (!resp.ok) {
                                return resp.json().then(text => {
                                    throw new Error(`${text.errorMessage}`)
                                })
                            } else {
                                return resp.json();
                            }
                        })
                        //menangkap data dari fetch

                        .then(json => {

                            // this.clearField();
                            swal({
                                title: `${json.errorMessage}`,
                                icon: "success",
                            })
                                .then((value) => {
                                    if(this.state.cari !== ""){
                                        this.searchPagination(this.state.page, this.state.limit);
                                    }else{
                                        this.getUsersPaging();
                                    }
                                    // $("#form-obat .close").click();
                                    // this.getObat();

                                })
                        })
                        .catch((e) => {
                            swal({
                                title: "perhatian",
                                text: `${e}`,
                                icon: "error",
                            });

                        });
                } else {
                    //   swal("Your imaginary file is safe!");
                }
            });
    }

    editAdminClicked = () => {
        this.setState({
            visible: ""
        })
        let obj = this.state
        if (obj.namaPengguna === "" || obj.nama === "" || obj.alamat === "" || obj.hp === "" || obj.email === "") {
            swal({
                title: "Isi semua data !!",
                icon: "warning",
                text: "Harap semua data diisi terlebih dahulu.."
            })
        } else {
            const changeProfil = {
                idUser: this.state.id,
                name: this.state.nama,
                address: this.state.alamat,
                phone: this.state.hp,
                email: this.state.email,
                username: this.state.namaPengguna
            }
            fetch(`http://localhost:8080/api/user-admin/update/`, {
                method: "put",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify(changeProfil)
            })
                .then(resp => {

                    if (!resp.ok) {
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json();
                    }
                })
                //menangkap data dari fetch

                .then(json => {

                    this.clearField();
                    swal({
                        title: `${json.errorMessage}`,
                        icon: "success",
                    })
                        .then((value) => {
                            $("#user .close").click();
                            if(this.state.cari !== ""){
                                this.searchPagination(this.state.page, this.state.limit);
                            }else{
                                this.getUsersPaging();
                            }
                            

                        })

                })
                .catch((e) => {
                    swal({
                        title: "perhatian",
                        text: `${e}`,
                        icon: "error",
                    });

                });
        }

    }


    createAdmin = () => {
        const { namaPengguna, nama, alamat, hp, email } = this.state
        console.log("btn", this.state.button);
        if (this.state.button === "Simpan") {
            if (namaPengguna === "" || nama === "" || alamat === "" || hp === "" || email === "") {
                swal({
                    title: "Isi semua data !!",
                    icon: "warning",
                    text: "Harap semua data diisi terlebih dahulu.."
                })
            } else {
                const dataAdmin = {

                    username: this.state.namaPengguna,
                    name: this.state.nama,
                    address: this.state.alamat,
                    phone: this.state.hp,
                    email: this.state.email,
                }
                console.log(dataAdmin);
                fetch(`http://localhost:8080/api/user-admin/`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json; ; charset=utf-8",
                        "Access-Control-Allow-Headers": "Authorization, Content-Type",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify(dataAdmin)
                })
                    .then(resp => {

                        if (!resp.ok) {
                            return resp.json().then(text => {
                                throw new Error(`${text.errorMessage}`)
                            })
                        } else {
                            return resp.json();

                        }
                    })
                    //menangkap data dari fetch
                    .then(json => {
                        swal({
                            title: `${json.errorMessage}`,
                            icon: "success",
                        })
                            .then((value) => {
                                this.clearField();
                                $("#user .close").click();
                                if(this.state.cari !== ""){
                                    this.searchPagination(this.state.page, this.state.limit);
                                }else{
                                    this.getUsersPaging();
                                }

                            })
                    })
                    .catch((e) => {
                        swal({
                            title: "perhatian",
                            text: `${e}`,
                            icon: "error",
                        });

                    });
            }

        } else {
            this.editAdminClicked();
        }
    }

    detailUserValue = (idUser) => {
        console.log(idUser);
        const cari = this.state.users.findIndex((idx) => idx.idUser === idUser);
        const detail = this.state.users[cari];
        console.log(detail);
        this.setState({
            id: detail.idUser,
            namaPengguna: detail.username,
            nama: detail.name,
            alamat: detail.address,
            hp: detail.phone,
            email: detail.email,
            tanggalRegistrasi: detail.registrationDate
        });
    }

    closeBtn = () => {
        $("#user-detail .close").click();
        // this.clearField();
    }


    componentDidMount() {
        this.getCountUser();
        this.getUsersPaging(this.state.page, this.state.limit);

    }

    nav = () => {
        if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Member") {
            this.props.history.push("/dashboard")
        }
    }
    render() {
        this.nav();
        const { id, nama, hp, alamat, email, namaPengguna, page, limit } = this.state
        return (
            <>
                <Container>
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <Div className="sb-sidenav-menu-heading">
                                Menu
                            </Div>
                            <Link to="/dashboard-admin">
                                <Active >
                                    <Menu icon="fas fa-tachometer-alt">
                                        Beranda
                                </Menu>
                                </Active>
                            </Link>
                            <Link to="/obat/admin">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                    Obat
                                </Menu>
                            </Link>
                            <Link to="/pengguna">
                                <Active active="active">
                                    <Menu icon="fas fa-user">
                                        Pengguna
                                    </Menu>
                                </Active>
                            </Link>
                            <Link to="/resep">
                                <Menu icon="fas fa-file-medical">
                                    Resep
                                </Menu>
                            </Link>
                            <Link to="/laporan">
                                <Menu icon="fas fa-book">
                                    Laporan
                                </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Div className="container-table">
                                <Div className="judul-pengguna">
                                    <TitlePage icon="fas fa-user" >
                                        Daftar Pengguna
                                    </TitlePage>
                                </Div>
                                {/* <div className="container"> */}
                                <div className="row">
                                    <div className="col-9 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                        <Input
                                            type="search"
                                            name="cari"
                                            className="form-control"
                                            placeholder="Cari ID, Nama, Status Pengguna"
                                            onChange={this.setValue}
                                        />
                                    </div>
                                    <div className="col-1">
                                        <Button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => { this.searchPagination() }}
                                        >
                                            <i className="fas fa-search" />
                                        </Button>
                                    </div>
                                    <div className="ml-5">
                                        <Button
                                            className="btn btn-primary"
                                            dataToogle="modal"
                                            dataTarget="#user"
                                            id="sunting"
                                            data-target="#exampleModal"
                                            onClick={() => {
                                                this.clearField();
                                                this.setState({
                                                    disabled: false,
                                                    button: "Simpan",
                                                    visible: "hidden",
                                                    type: "hidden"
                                                })
                                            }}
                                        >Tambah</Button>
                                    </div>
                                </div>
                                {/* </div>  */}
                                <Div className="tabel">
                                    <table className="table table-bordered table-striped">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama Pengguna</th>
                                                <th scope="col">Nama Lengkap</th>
                                                <th scope="col">Status</th>
                                                <th
                                                    style={{ textAlign: "center" }}
                                                    scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.users.length === 0 ?
                                                <tr>
                                                    <td
                                                        colSpan="4"
                                                        align="center"
                                                    >Data kosong</td>
                                                </tr>
                                                :
                                                this.state.users.map((a, index) => {
                                                    return (

                                                        <tr key={index}>
                                                            <td>{(((page * limit) - limit + 1) + index)}</td>
                                                            <td>{a.username}</td>
                                                            <td>{a.name}</td>
                                                            <td>{a.role}</td>
                                                            <td>
                                                                <center>
                                                                    {/* <Div style={{ padding: "0 7vh 0 7vh" }}> */}
                                                                    {a.role === "Admin" ?
                                                                        a.idUser === this.props.user.idUser ?
                                                                            <>
                                                                                {/* <center> */}
                                                                                <Tooltip isi="Detail">
                                                                                    <Button

                                                                                        className="btn btn-secondary center"
                                                                                        dataToogle="modal"
                                                                                        dataTarget="#user-detail"
                                                                                        style={{ float: "left", marginLeft: "12vh" }}
                                                                                        onClick={() => { this.detailUserValue(a.idUser) }}
                                                                                    >
                                                                                        <I className="fas fa-info-circle"></I>
                                                                                    </Button>
                                                                                </Tooltip>
                                                                                <ReactTooltip />

                                                                                <Tooltip isi="Sunting">
                                                                                    <Button
                                                                                        className="btn btn-warning text-white center"
                                                                                        dataToogle="modal"
                                                                                        dataTarget="#user"
                                                                                        style={{ float: "left", marginLeft: "3vh" }}
                                                                                        onClick={() => { this.adminValue(a.idUser); this.setState({ visible: "", type: "text" }) }}
                                                                                    ><I className="fas fa-edit"></I></Button>
                                                                                </Tooltip>
                                                                                <ReactTooltip />
                                                                                {/* </center> */}
                                                                            </>

                                                                            :
                                                                            <>
                                                                                <Tooltip isi="Detail">
                                                                                    <Button
                                                                                        className="btn btn-secondary center"
                                                                                        dataToogle="modal"
                                                                                        dataTarget="#user-detail"

                                                                                        onClick={() => { this.detailUserValue(a.idUser) }}
                                                                                    >
                                                                                        <I className="fas fa-info-circle"></I>
                                                                                    </Button>
                                                                                </Tooltip>
                                                                                <ReactTooltip />
                                                                            </>
                                                                        :
                                                                        <>
                                                                            {/* <center> */}
                                                                            <center>
                                                                                <Tooltip isi="Detail">
                                                                                    <Button
                                                                                        className="btn btn-secondary"
                                                                                        dataToogle="modal"
                                                                                        dataTarget="#user-detail"
                                                                                        style={{ float: "left", marginLeft: "10vh" }}
                                                                                        onClick={() => { this.detailUserValue(a.idUser) }}
                                                                                    >
                                                                                        <I className="fas fa-info-circle"></I>
                                                                                    </Button>
                                                                                </Tooltip>
                                                                                <ReactTooltip />
                                                                                {/* <Div
                                                                                    className="a detail-btn"
                                                                                > */}
                                                                                <Tooltip isi="Reset Kata Sandi">
                                                                                    <Button
                                                                                        // id="sunting"
                                                                                        className="btn btn-warning text-white"
                                                                                        style={{ float: "left", marginLeft: "3vh" }}
                                                                                        onClick={() => { this.resetPassword(a.idUser) }}>
                                                                                        <I className="fas fa-key"></I>
                                                                                    </Button>
                                                                                </Tooltip>
                                                                                <ReactTooltip />
                                                                                {/* </Div> */}
                                                                            </center>
                                                                        </>

                                                                    }
                                                                    {/* </Div> */}
                                                                    {/* <Div className="hapusData">
                                                                    <Button id="hapus" className="btn btn-danger" onClick={() => { this.hapusData(index) }}>Hapus</Button>
                                                                </Div> */}
                                                                    {/* </Div> */}
                                                                </center>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    {this.state.users.length !== 0  &&
                                   

                                    <Div className="pagination">
                                        <Pagination count={this.state.count} page={this.state.page}
                                            onChange={this.handleChange}
                                        />

                                    </Div>
                                    }
                                </Div>
                            </Div>
                            <Footer>
                                Copyright © Rilo 2021
                            </Footer>
                        </Div>
                    </Div>
                </Container>
                <Modal id="user-detail">
                    <ModalHeader className="close">
                        Data Pengguna
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-3">
                                    ID Pengguna
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.id}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Nama Pengguna
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.namaPengguna}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Nama
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.nama}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Nomor Telepon
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.hp}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Email
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.email}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Alamat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.alamat}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Registrasi
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-4 row">
                                    <Div className="col-md-12">
                                        {this.state.tanggalRegistrasi}
                                    </Div>
                                </Div>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-secondary" onClick={() => this.closeBtn()}>Tutup</Button>
                    </Div>
                </Modal>
                {/* Modal */}
                <Modal id="user">
                    <ModalHeader className="close">
                        Profil
                    </ModalHeader>
                    <Div className="modal-body">
                        <Label className="small mb-1 required" hidden={this.state.visible}>ID User</Label>
                        <Input
                            name="id"
                            value={id}
                            type={this.state.type}
                            placeholder="ID User"
                            className="form-control"
                            onChange={this.setValue}
                            disabled={this.state.disabled} />
                        <Label className="small mb-1 required">Nama Pengguna</Label>
                        <Input
                            type="text"
                            name="namaPengguna"
                            className="form-control"
                            placeholder="Masukkan Nama Pengguna"
                            value={namaPengguna}
                            onChange={this.setValue} />
                        <Label className="small mb-1 required">Nama Lengkap</Label>
                        <Input
                            type="text"
                            name="nama"
                            className="form-control"
                            placeholder="Masukkan Nama"
                            value={nama}
                            onChange={this.setValue} />
                        <Label className="small mb-1 required">Alamat</Label>
                        <TextArea
                            type="text"
                            name="alamat"
                            className="form-control"
                            placeholder="Masukkan Alamat"
                            value={alamat}
                            onChange={this.setValue} />
                        <Div className="form-row">
                            <FormRow>
                                <Label className="small mb-1 required">Telepon</Label>
                                <Input
                                    type="number"
                                    name="hp"
                                    min="0"
                                    className="form-control"
                                    placeholder="Masukkan Nomor Telepon"
                                    value={hp}
                                    onChange={this.setValue} />
                            </FormRow>
                            <FormRow>
                                <Label className="small mb-1 required">Email</Label>
                                <Input
                                    type="text"
                                    name="email"
                                    className="form-control"
                                    placeholder="Masukkan Email"
                                    value={email}
                                    onChange={this.setValue} />
                            </FormRow>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-primary" onClick={() => this.createAdmin()}>{this.state.button}</Button>
                    </Div>
                </Modal>

            </>
        );
    }

}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    login: state.AReducer.dataUser,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser,
    checkLogin: state.AReducer.isLogin,

})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({

    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })
})

export default (connect(mapStateToProps, mapDispatchToProps))(User);