import React, { Component } from 'react';
import { Menu, MenuTitle, Nav, Active, Header, Div, Container, TitlePage, Input, I, Tooltip, Button, Modal, ModalHeader, FormRow, Label, TextArea, Select, Option } from "../../../component"
import Footer from '../../../template/footer';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import swal from 'sweetalert';
import Pagination from '@material-ui/lab/Pagination';
import ReactTooltip from 'react-tooltip';
import konversiRupiah from '../../../component/util';
// import _ from 'lodash';

class ObatAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            limit: 5,
            count: 0,
            obat: [],
            jenisObatList: [],
            idObat: "",
            hapus: {},
            idObat: "",
            namaObat: "",
            stok: "",
            harga: "",
            dosis: "",
            idJenis: "",
            jenis: "",
            button: "Simpan",
            idJenisObat: "",
            disabled: false,
            type: "hidden",
            operator: "",
            hidden: "hidden",
            stokTerbaru: "",
            time: Date.now(),
            cari: "",
            // kondisiCari:false,
            pageNow:1
        }
    }


    setValue = el => {
        if(el.target.name === "cari" && el.target.value === ""){
            this.getObatListWithPage(this.state.pageNow, this.state.limit);
            this.getCount();
        }

        this.setState({
            [el.target.name]: el.target.value,
        })

    }

    searchPagination = (page, limit) => {
        if(this.state.cari === ""){
            swal({
                title: "Perhatian",
                icon: "warning",
                text: "Harap masukkan kata pencarian"
            })
        }else{
            this.setState({
                // kondisiCari : true,
                page:page
            })
            let keyword = this.state.cari
            if (page){}else { page = 1 }
            Promise.all([
                fetch(`http://localhost:8080/api/search/obat/?idUser=${this.props.user.idUser}&keyword=${keyword}&page=${page}&limit=${this.state.limit}`),
                fetch('http://localhost:8080/api/search-counter/obat/?idUser='+this.props.user.idUser+'&keyword=' + keyword),
            ])
            .then(([response, response2]) => Promise.all([response.json(), response2.json()]))
            .then(([json, json2]) => {
                
                this.setState({
                    obat: json, count: Math.ceil(Number(json2.countSearch) / this.state.limit),
                    page: page
                    
                })
                console.log("hitung", this.state.count);
                console.log("hitung", json2);

                
            })
            
        }
            
        }
        
        getCount = () => {
            fetch(`http://localhost:8080/api/count/?idUser=${this.props.user.idUser}`, {
                method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    count: Math.ceil(Number(json) / this.state.limit)
                });
            })
            .catch((e) => {
                alert(e);
            });
    }

    // getObat = () => {
    //     fetch('http://localhost:8080/api/obat/', {
    //         method: "get",
    //         headers: {
    //             "Content-Type": "application/json; ; charset=utf-8",
    //             "Access-Control-Allow-Headers": "Authorization, Content-Type",
    //             "Access-Control-Allow-Origin": "*"
    //         }
    //     })
    //         .then(response => response.json())
    //         .then(json => {
    //             this.setState({ obat: json });
    //         })
    //         .catch((e) => {
    //             console.log(e);
    //             console.log("tes", this.state.obat);
    //             alert("Failed fetching data!!");
    //         });
    // }
    

    handleChange = (event, value) => {
        this.setState({
            page: value
        })
        
            if(this.state.cari !== ""){
                this.searchPagination(value, this.state.limit)
            }else{
                this.getObatListWithPage(value, 5);
                this.getCount();
            }
        
        
        console.log("value", value);

    }

    getObatListWithPage = (page, limit) => {
        if (page){}else { page = this.state.page }
        if(limit){}else{limit = this.state.limit}
            
        // else { }
        
        fetch(`http://localhost:8080/api/obat-pagination/?page=` + page + '&limit=' + limit + '', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ 
                    obat: json,
                    page: page
                 });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    closeBtn = () => {
        $("#obat .close").click();
        this.clearField();
    }

    updateObatValue = (idObat) => {
        console.log(idObat);
        const cari = this.state.obat.findIndex((idx) => idx.idObat === idObat);
        const detail = this.state.obat[cari];
        console.log(detail);
        this.clearUpdateStok();
        this.setState({
            idObat: detail.idObat,
            namaObat: detail.namaObat,
            harga: detail.harga,
            stok: detail.stok,
            jenis: detail.jenis,
            dosis: detail.dosis,
            idJenisObat: detail.idJenisObat,
            button: "Sunting",
            disabled: true,
            type: "number",
            hidden: ""
        });
    }

    detailObatValue = (idObat) => {
        console.log(idObat);
        const cari = this.state.obat.findIndex((idx) => idx.idObat === idObat);
        const detail = this.state.obat[cari];
        console.log(detail);
        this.setState({
            idObat: detail.idObat,
            namaObat: detail.namaObat,
            harga: detail.harga,
            stok: detail.stok,
            jenis: detail.jenis,
            dosis: detail.dosis,
            idJenisObat: detail.idJenisObat,

        });
    }

    updateObat = () => {

        const { idObat, namaObat, stok, harga, dosis, idJenisObat, operator, stokTerbaru } = this.state

        if (idObat === "" || namaObat === "" || stok === "" || harga === "" || dosis === "" || idJenisObat === "") {
            swal({
                title: "Isi semua data !!",
                icon: "warning",
                text: "Harap semua data diisi terlebih dahulu.."
            })
        } else if (stokTerbaru <= -1) {
            let stokUpdate
            stokUpdate = stok
            swal({
                title: "Perubahan stok tidak boleh ada (-)",
                icon: "warning",
                text: "Harap data diisi dengan benar"
            })
        } else {
            let stokUpdate
            console.log("ini stok", stokUpdate);

            if (operator === "+") {
                stokUpdate = stok + Number(stokTerbaru)
            } else if (operator === "-") {
                stokUpdate = stok - Number(stokTerbaru)
            } else if (operator === "") {
                stokUpdate = stok
            }
            const changeObat = {
                idObat: this.state.idObat,
                namaObat: this.state.namaObat,
                stok: stokUpdate,
                harga: this.state.harga,
                dosis: this.state.dosis,
                idJenisObat: this.state.idJenisObat
            }
            fetch(`http://localhost:8080/api/update/obat/`, {
                method: "put",
                headers: {
                    "Content-Type": "application/json; ; charset=utf-8",
                    "Access-Control-Allow-Headers": "Authorization, Content-Type",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify(changeObat)
            })
                .then(resp => {

                    if (!resp.ok) {
                        return resp.json().then(text => {
                            throw new Error(`${text.errorMessage}`)
                        })
                    } else {
                        return resp.json();
                    }
                })
                //menangkap data dari fetch

                .then(json => {

                    this.clearField();
                    swal({
                        title: `${json.errorMessage}`,
                        icon: "success",
                    })
                        .then((value) => {
                            $("#form-obat .close").click();
                            if(this.state.cari !== ""){
                                this.searchPagination(this.state.page, this.state.limit);
                            }else{
                                this.getObatListWithPage();
                            }

                        })

                })
                .catch((e) => {
                    swal({
                        title: "perhatian",
                        text: `${e}`,
                        icon: "error",
                    });

                });
        }

    }

    createObat = () => {
        const { idObat, namaObat, stok, harga, dosis, idJenisObat, operator, stokTerbaru } = this.state

        if (this.state.button === "Simpan") {
            if (idObat === "" || namaObat === "" || stok === "" || harga === "" || dosis === "" || idJenisObat === "") {
                swal({
                    title: "Isi semua data !!",
                    icon: "warning",
                    text: "Harap semua data diisi terlebih dahulu.."
                })
            } else {
                const dataObat = {
                    idObat: this.state.idObat,
                    namaObat: this.state.namaObat,
                    stok: this.state.stok,
                    harga: this.state.harga,
                    dosis: this.state.dosis,
                    idJenisObat: this.state.idJenisObat
                }
                fetch(`http://localhost:8080/api/create/obat/`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json; ; charset=utf-8",
                        "Access-Control-Allow-Headers": "Authorization, Content-Type",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify(dataObat)
                })
                    .then(resp => {

                        if (!resp.ok) {
                            return resp.json().then(text => {
                                throw new Error(`${text.errorMessage}`)
                            })
                        } else {
                            return resp.json();

                        }
                    })
                    //menangkap data dari fetch
                    .then(json => {
                        swal({
                            title: `${json.errorMessage}`,
                            icon: "success",
                        })
                            .then((value) => {
                                this.clearField();
                                $("#form-obat .close").click();
                                if(this.state.cari !== ""){
                                    this.searchPagination(this.state.page, this.state.limit);
                                }else{
                                    this.getObatListWithPage();
                                }

                            })
                    })
                    .catch((e) => {
                        swal({
                            title: "perhatian",
                            text: `${e}`,
                            icon: "error",
                        });

                    });
            }
        } else {
            this.updateObat();
        }
    }

    getJenisObat = () => {
        console.log("tessss", this.state.jenisObatList);
        fetch('http://localhost:8080/api/jenis-obat/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ jenisObatList: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }


    createBtn = () => {
        this.clearField();
        this.setState({
            button: "Simpan",
            disabled: false,
            type: "hidden",
            hidden: "hidden"
        })
    }

    clearField = () => {
        this.setState({
            idObat: "",
            namaObat: "",
            harga: "",
            stok: "",
            idJenis: "",
            dosis: "",
            operator: "",
            stokTerbaru: "",
            idJenisObat: ""
        });
    }

    clearUpdateStok = () => {
        this.setState({
            operator: "",
            stokTerbaru: ""
        })
    }

    

    updateStatusObat = (id, status) => {
        const dataStatus = {
            idObat: id,
            statusObat: status,
        };
        swal({
            title: "Apakah anda yakin ?",
            text: "Status obat akan dirubah...",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        }).then((konfirmasi) => {
            if (konfirmasi) {
                fetch("http://localhost:8080/api/update-status-obat/", {
                    method: "put",
                    headers: {
                        "Content-Type": "application/json; ; charset=utf-8",
                        "Access-Control-Allow-Headers": "Authorization, Content-Type",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify(dataStatus),
                })
                    .then((response) => response.json())
                    .then((json) => {
                        if (typeof json.errorMessage !== "undefined") {
                            swal("Berhasil !", json.errorMessage, "success");
                            if(this.state.cari !== ""){
                                this.searchPagination(this.state.page, this.state.limit);
                            }else{
                                this.getObatListWithPage();
                            }
                        }
                    })
                    .catch((e) => { });
            } else {
                swal("Batal !", "Ubah status obat dibatalkan", "error");
            }
        });
    };

    componentDidMount() {
        // this.getObat();
        this.getCount();
        this.getObatListWithPage(this.state.page, this.state.limit);
        this.getJenisObat();

    }

 

    nav = () => {
        if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Member") {
            this.props.history.push("/dashboard")
        }
    }

    render() {
        
        this.nav();
        const { idObat, namaObat, stok, harga, dosis, jenis, idJenisObat, operator, stokTerbaru } = this.state
        return (
            <>
                <Container>
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                            </MenuTitle>
                            <Link to="/dashboard-admin">
                                <Menu icon="fas fa-tachometer-alt">
                                    Beranda
                            </Menu>
                            </Link>
                            <Link to="/obat/admin">
                                <Active active="active">
                                    <Menu icon="fas fa-prescription-bottle-alt">
                                        Obat
                                </Menu>
                                </Active>
                            </Link>
                            <Link to="/pengguna">
                                <Menu icon="fas fa-user">
                                    Pengguna
                                </Menu>
                            </Link>
                            <Link to="/resep">
                                <Menu icon="fas fa-file-medical">
                                    Resep
                                </Menu>
                            </Link>
                            <Link to="/laporan">
                                <Menu icon="fas fa-book">
                                    Laporan
                                </Menu>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Div className="container-table">
                                <Div className="judul-pengguna">
                                    <TitlePage icon="fas fa-user" >
                                        Daftar Obat
                                    </TitlePage>
                                </Div>
                                {/* <Div className="container"> */}
                                    <Div className="row">
                                        <Div className="col-9 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                            <Input
                                                type="search"
                                                name="cari"
                                                className="form-control"
                                                placeholder="Cari ID, Nama, Harga Obat"
                                                onChange={this.setValue}
                                            />
                                        </Div>
                                        <Div className="col-1">
                                            <Button
                                                type="button"
                                                className="btn btn-primary"
                                                onClick={() => { this.searchPagination() }}
                                            >
                                                <I className="fas fa-search" />
                                            </Button>
                                        </Div>
                                        <Div className="ml-5">
                                            <Button
                                                className="btn btn-primary"
                                                dataToogle="modal"
                                                dataTarget="#form-obat"
                                                id="sunting"
                                                onClick={() => { this.createBtn() }}
                                                data-target="#exampleModal">
                                                Tambah
                                            </Button>
                                        </Div>
                                    </Div>
                                {/* </Div> */}

                                <Div className="tabel">
                                    <table className="table table-bordered table-striped">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">ID Obat</th>
                                                <th scope="col">Nama Obat</th>
                                                <th scope="col">Stok</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Jenis Obat</th>
                                                {/* <th>status</th> */}
                                                {/* <th scope="col">Tanggal Registrasi</th> */}
                                                <th style={{ textAlign: "center" }} >Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            {this.state.obat.length >0?
                                            
                                            this.state.obat.map((a, index) => {
                                                return (
                                                        
                                                    <tr key={index}>
                                                        <td>{((this.state.page - 1) * this.state.limit + 1)+index}</td>
                                                        <td>{a.idObat}</td>
                                                        <td>{a.namaObat}</td>
                                                        <td>{a.stok}</td>
                                                        <td>{konversiRupiah(a.harga)}</td>
                                                        <td>{a.jenis}</td>
                                                        {/* <td>{a.statusObat}</td> */}
                                                        <td >
                                                            <center>
                                                                <Div style={{ float: "left", marginLeft: "10vh" }}>
                                                                    <Tooltip isi="Detail">

                                                                        <Button
                                                                            className="btn btn-secondary"
                                                                            dataToogle="modal"
                                                                            dataTarget="#obat"
                                                                            data-target="#exampleModal"
                                                                            onClick={() => { this.detailObatValue(a.idObat) }}
                                                                        ><I className="fas fa-info-circle"></I></Button>
                                                                    </Tooltip>
                                                                    <ReactTooltip />
                                                                </Div>
                                                                <Div style={{ float: "left", paddingLeft: "2vh" }}>
                                                                    <Tooltip isi="Sunting Obat">
                                                                        <Button
                                                                            className="btn btn-warning"
                                                                            dataToogle="modal"
                                                                            dataTarget="#form-obat"
                                                                            data-target="#exampleModal"
                                                                            hidden={a.statusObat === "Active" ? false : true}
                                                                            onClick={() => { this.updateObatValue(a.idObat) }}
                                                                        ><I className="far fa-edit"></I></Button>
                                                                    </Tooltip>
                                                                    <ReactTooltip />
                                                                </Div>
                                                                <Div style={{ float: "left", paddingLeft: "2vh" }}>
                                                                    <Tooltip isi="Non Aktifkan">
                                                                        <Button
                                                                            hidden={a.statusObat === "Active" ? false : true}
                                                                            onClick={() => { this.updateStatusObat(a.idObat, "Inactive") }}
                                                                            className="btn btn-danger"
                                                                        ><I className="far fa-times-circle"></I>
                                                                        </Button>
                                                                    </Tooltip>
                                                                    <ReactTooltip />
                                                                    <Tooltip isi="Aktifkan">
                                                                        <Button
                                                                            hidden={a.statusObat === "Inactive" ? false : true}
                                                                            onClick={() => { this.updateStatusObat(a.idObat, "Active") }}
                                                                            className="btn btn-success"
                                                                        ><I className="far fa-check-circle"></I>
                                                                        </Button>
                                                                    </Tooltip>
                                                                    <ReactTooltip />
                                                                </Div>
                                                            </center>
                                                        </td>
                                                    </tr>
                                                )
                                                
                                            })
                                            :
                                                <>
                                                <tr>
                                                    <td
                                                        colSpan="6"
                                                        align="center"
                                                        >Data kosong
                                                    </td>
                                                </tr>
                                                </>
                                            }
                                        </tbody>
                                    </table>
                                    {this.state.obat.length !== 0  &&
                                    <Div className="pagination" >
                                        
                                        <Pagination count={this.state.count} page={this.state.page}
                                            onChange={this.handleChange}
                                        />
                                    </Div>
                                    }
                                </Div>
                            </Div>
                            <Footer>
                                Copyright © Rilo 2021
                            </Footer>
                        </Div>
                    </Div>
                </Container>
                <Modal id="obat">
                    <ModalHeader className="close">
                        Obat
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-2">
                                    ID Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.idObat}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Nama Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.namaObat}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Stok
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.stok}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Harga
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {konversiRupiah(this.state.harga)}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Jenis Obat
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.jenis}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-2">
                                    Dosis
                                </Div>
                                <Div className="col-md-1">:</Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        {this.state.dosis}
                                    </Div>
                                </Div>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-secondary" onClick={() => this.closeBtn()}>Tutup</Button>
                    </Div>
                </Modal>
                {/* Modal Create dan Update */}
                <Modal id="form-obat">
                    <ModalHeader className="close">
                        Data Obat
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">ID Obat</Label>
                            <Input
                                name="idObat"
                                value={idObat}
                                type="text"
                                placeholder="ID Obat"
                                className="form-control col-2"
                                onChange={this.setValue}
                                disabled={this.state.disabled} />
                        </Div>
                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">Nama Obat</Label>
                            <Input
                                type="text"
                                name="namaObat"
                                className="form-control col-5"
                                placeholder="Masukkan Nama Obat"
                                value={namaObat}
                                onChange={this.setValue} />
                        </Div>
                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">Stok</Label>
                            <Div className="row g-10">
                                <Div className="col-5">
                                    <Input
                                        type="number"
                                        min="1"
                                        name="stok"
                                        className="form-control"
                                        placeholder="Stok"
                                        value={stok}
                                        disabled={this.state.disabled}
                                        onChange={this.setValue} />
                                </Div>
                                <Div className="col-auto">
                                    <select name="operator" hidden={this.state.hidden} value={operator} onChange={this.setValue} className="form-control " aria-label="Default select example">
                                        <option value="">Pilih</option>
                                        <option value="+">+</option>
                                        <option value="-">-</option>
                                    </select>
                                </Div>
                                <Div className="col-3">
                                    <Input name="stokTerbaru" value={stokTerbaru} onChange={this.setValue} type={this.state.type} min="1" className="form-control" id="inputPassword2" placeholder="Stok Terbaru" />
                                </Div>
                            </Div>
                        </Div>

                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">Harga</Label>
                            <Input
                                type="number"
                                min="1"
                                name="harga"
                                className="form-control col-5"
                                placeholder="Masukkan Harga Rp. "
                                value={harga}
                                onChange={this.setValue} />
                        </Div>
                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">Jenis Obat</Label>
                            <Select name="idJenisObat" value={idJenisObat} onChange={this.setValue} className="form-control col-5" >
                                <Option value="">Pilih Jenis Obat</Option>
                                {
                                    this.state.jenisObatList.map(
                                        (Item, idx) =>
                                            <Option value={Item.idJenisObat} key={idx}>{Item.jenisObat}</Option>
                                    )
                                }
                            </Select>
                        </Div>
                        <Div className="form-group row">
                            <Label className="col-sm-2 col-form-label required">Dosis</Label>
                            <TextArea
                                type="text"
                                name="dosis"
                                className="form-control col-8"
                                placeholder="Masukkan keterangan dosis"
                                value={dosis}
                                onChange={this.setValue} />
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-primary" onClick={() => this.createObat()}>{this.state.button}</Button>
                    </Div>
                </Modal>

            </>
        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    login: state.AReducer.dataUser,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser,
    checkLogin: state.AReducer.isLogin


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({

    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })
})

export default (connect(mapStateToProps, mapDispatchToProps))(ObatAdmin);
