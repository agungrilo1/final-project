import React, { Component } from 'react';
import { Menu, MenuTitle, Nav, Active, Header, I, Div, Container } from "../../../component"
import Footer from '../../../template/footer';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import konversiRupiah from '../../../component/util';


class DashboardAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beranda: {},
            display:"none",
            selected: this.handleSetDate(new Date())

        }
        this.handleDateChange = (date) => {
            let dateValue = this.handleSetDate(date)
            this.setState({
                selected: dateValue
            }, () => this.getBeranda(dateValue))
        }
    }



    handleSetDate = dateValue => {
        let current_datetime;
        if (dateValue === null) {
            dateValue = new Date()
            current_datetime = dateValue
        } else {
            current_datetime = dateValue
        }
        let year = current_datetime.getFullYear();
        let month = ("0" + (current_datetime.getMonth() + 1)).slice(-2)
        let day = ("0" + current_datetime.getDate()).slice(-2)
        let formatted_date = year + "-" + month + "-" + day
        console.log("cek", year, month, day, formatted_date);
        console.log("ini tanggal", (current_datetime.getDate()));
        return formatted_date
    }

    getBeranda = formatted_date => {
        const { selected } = this.state
        fetch(`http://localhost:8080/api/home/?tglTransaksi=${formatted_date}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({ beranda: json });
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    componentDidMount = () => {
        let dateValue = this.handleSetDate(new Date())
        this.getBeranda(dateValue);
    }

    closeInfo=()=>{
        this.setState({
            display:""
        })
    }

    nav = () => {
        console.log("password",this.props.user.password)
        if(this.props.user.password === "User123"){
            this.props.history.push("/password")
        }else if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Member") {
            this.props.history.push("/dashboard")
        }
    }

    render() {
        this.nav();
        const { beranda } = this.state

        return (
            <Container>
                <Header />
                <Div id="layoutSidenav">
                    <Nav>
                        <MenuTitle>
                            Menu
                            </MenuTitle>
                        <Link to="/dashboard/admin">
                            <Active active="active">
                                <Menu icon="fas fa-tachometer-alt">
                                    Beranda
                                </Menu>
                            </Active>
                        </Link>
                        <Link to="/obat/admin">
                            <Menu icon="fas fa-prescription-bottle-alt">
                                Obat
                                </Menu>
                        </Link>
                        <Link to="/pengguna">
                            <Menu icon="fas fa-user">
                                Pengguna
                                </Menu>
                        </Link>
                        <Link to="/resep">
                            <Menu icon="fas fa-file-medical">
                                Resep
                                </Menu>
                        </Link>
                        <Link to="/laporan">
                            <Menu icon="fas fa-book">
                                Laporan
                                </Menu>
                        </Link>
                    </Nav>
                    <Div id="layoutSidenav_content">
                    <Div id="alert" className="alert alert-info alert-dismissible fade show" style={{}}  role="alert">
                                <strong>Perhatian !!</strong> Pada halaman ini berisikan laporan penjualan berupa jumlah transaksi dan jumlah pendapatan secara harian
                                <button type="button" className="close" onClick={()=>{this.closeInfo()}}data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </Div>
                        <Div className="container-fluid">
                            <Div className="container-fluid" style={{ backgroundColor: "gray", borderRadius: "10vh", color: "white",display:this.state.display }}>
                                <h2 className="title-dashboard-member">SELAMAT DATANG</h2>
                            </Div>
                            <Div className="container">
                            </Div>
                            <center>
                                <Div className="mt-5">
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            // value={this.handleSetDate(new Date())}
                                            value={(this.state.selected)}
                                            onChange={date => this.handleDateChange(date)}
                                            format="yyyy-MM-dd"
                                        />
                                    </MuiPickersUtilsProvider>
                                </Div>
                            </center>
                            <Div className="row justify-content-md-center mt-5">
                            <Div className="col-xl-4">
                                    
                                </Div>
                            </Div>
                            <Div className="row justify-content-md-center ">
                                <Div className="col-xl-4">
                                    <Div className="card obat m-b-30">
                                        <I className="fas fa-user fa-6x mt-2" style={{ alignSelf: "center" }}></I>
                                        <Div className="card-body">
                                            <h4 className="text-center font-16 text-success">Jumlah Transaksi</h4>
                                            {beranda.jumlahPengunjung === undefined ?
                                                <h1 style={{ textAlign: "center" }}>0</h1>
                                                :
                                                <h1 className="text-center">{beranda.jumlahPengunjung}</h1>
                                            }
                                        </Div>
                                    </Div>
                                </Div>
                                <Div className="col-xl-4">
                                    <Div className="card obat m-b-30">
                                        {/* <img className="card-img-top" src={gambar} height={250} alt="Buku" /> */}
                                        <I className="fas fa-money-bill-alt fa-6x" style={{ alignSelf: "center" }}></I>
                                        <Div className="card-body">
                                            <h4 className="font-16 text-center mt-2 text-success">Jumlah Pendapatan</h4>
                                            {beranda.totalPendapatan === undefined ?
                                                <h1 style={{ textAlign: "center" }}>Rp. 0</h1>
                                                :
                                                <h1 style={{ textAlign: "center" }}>{konversiRupiah(beranda.totalPendapatan)}</h1>
                                            }
                                        </Div>
                                    </Div>
                                </Div>
                            </Div>
                        </Div>
                        <Footer>
                            Copyright © Rilo 2021
                        </Footer>
                    </Div>
                </Div>
            </Container>
        );
    }
}

//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    checkLogin: state.AReducer.isLogin,
    user: state.AReducer.dataUser


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({


})

export default (connect(mapStateToProps, mapDispatchToProps))(DashboardAdmin);