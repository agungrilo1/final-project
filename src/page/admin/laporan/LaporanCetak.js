import React, { Component } from 'react';
import {Div} from '../../../component';
import kop from '../../../asset/kop.png';

class LaporanCetak extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <>
                <center>
                    <Div className="card-header" style={{background:"white"}}>
                        <img src={kop} width={1250} alt="Kop Surat" />
                    </Div>
                    <table className="table table-bordered" style={{width:"93%"}}>
                                            <thead className="thead-dark">
                                                <th >No</th>
                                                    <th >Nama Pembeli</th>
                                                    <th >Tanggal Transaksi</th>
                                                    <th >Nama Obat</th>
                                                    <th >Jumlah Barang</th>
                                                    <th >Status</th>
                                                    <th >Harga</th>
                                            </thead>
                                            <tbody>
                                            {this.props.laporan.length === 0 ?
                                                    <tr>
                                                        <td
                                                            colSpan="7"
                                                            align="center"
                                                            >Data kosong
                                                        </td>
                                                    </tr>
                                                :
                                                this.props.laporan.map((a, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index+1}</td>
                                                            <td>{a.name}</td>
                                                            <td>{a.tglTransaksi}</td>
                                                            <td>{a.tglKonfirmasi}</td>
                                                            <td >
                                                                {a.obatList.map((b, index) => {
                                                                    return (

                                                                        <div key={index}>
                                                                            <p>
                                                                                - {b.namaObat}
                                                                            </p>
                                                                        </div>
                                                                    )
                                                                })
                                                                }
                                                            </td>
                                                            <td >{a.obatList.length} Jenis</td>
                                                            <td>{a.statusLaporan}</td>
                                                            <td>Rp. {a.total}</td>
                                                            
                                                        </tr>
                                                    )
                                                })
                                                }
                                            </tbody>
                                        </table>
                </center>
            </>
         );
    }
}
 
export default LaporanCetak;