import React, { Component } from 'react';
import { Menu, Main, MenuTitle, Nav, Active, Header, Div, Container, TitlePage, Input, I, Tooltip, Button, Modal, ModalHeader, FormRow, Label, TextArea } from "../../../component"
import Footer from '../../../template/footer';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import swal from 'sweetalert';
import Pagination from '@material-ui/lab/Pagination';
import ReactToPrint from 'react-to-print';
import LaporanCetak from './LaporanCetak';
import ReactTooltip from 'react-tooltip';
import konversiRupiah from '../../../component/util';

class Laporan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // time: Date.now(),
            cetakLaporan: [],
            laporan: [],
            obatList: [],
            idLaporan: "",
            nama: "",
            tanggalTransaksi: "",
            tanggalKonfirmasi:"",
            total: 0,
            ongkir: 0,
            harga: 0,
            status: "",
            //pagination
            limit: 5,
            page: 1,
            count: 0,
            total: "",
            cari1: "",
            cari2: "",
            cari3: "",
            kondisiCari: false,
            pageNow: 1

        }
    }


    getReportPrint = () => {
        let keyword1 = this.state.cari1
        let keyword2 = this.state.cari2
        let keyword3 = this.state.cari3
        fetch(`http://localhost:8080/api/report-print/?keyword1=${keyword1}&keyword2=${keyword2}&keyword3=${keyword3} `, {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    cetakLaporan: json,
                });
                console.log("ini json laporan", json);
            })
            .catch((e) => {
                console.log(e);
                alert("Failed fetching data!!");
            });
    }

    getReport = (page, limit) => {
        // if (page){}else { page = this.state.page }
        // if(limit){}else{limit = this.state.limit}
        fetch('http://localhost:8080/api/laporan/?limit=' + limit + '&page=' + page + '', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    laporan: json,
                    page: page
                });
                this.getReportPrint();
                console.log("ini json laporan", json);
            })
            .catch((e) => {
                alert("Failed fetching data!!");
            });
    }

    getCountReport = () => {
        fetch('http://localhost:8080/api/report-count/', {
            method: "get",
            headers: {
                "Content-Type": "application/json; ; charset=utf-8",
                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    count: Math.ceil(Number(json) / this.state.limit)
                });
                console.log("ini json laporan", json);
            })
            .catch((e) => {
                console.log(e);
                console.log("tes", this.state.obat);
                alert("Failed fetching data!!");
            });
    }

    handleChange = (event, value) => {
        this.setState({
            page: value
        })
        console.log("value", value);
        if (this.state.cari1 !== "" || this.state.cari2 !== "" || this.state.cari3 !== "") {
            this.searchPagination(value, this.state.limit);
        } else {
            this.getReport(value, 5);
            this.getCountReport();
        }

    }

    closeBtn = () => {
        $("#obat .close").click();
    }


    detailLaporanValue = (idLaporan) => {
        console.log(idLaporan);
        const cari = this.state.laporan.findIndex((idx) => idx.idLaporan === idLaporan);
        const detail = this.state.laporan[cari];
        console.log(detail);
        this.setState({
            idLaporan: detail.idLaporan,
            nama: detail.name,
            tanggalTransaksi: detail.tglTransaksi,
            tanggalKonfirmasi: detail.tglKonfirmasi,
            status: detail.statusLaporan,
            bayar: detail.total,
            ongkir: detail.ongkir,
            obatList: detail.obatList
        });
    }


    searchPagination = (page, limit) => {
        if (this.state.cari1 === "" && this.state.cari2 === "" && this.state.cari3 === "") {
            swal({
                title: "Perhatian",
                icon: "warning",
                text: "Harap masukkan kata pencarian"
            })
        } else {
            this.setState({
                kondisiCari: true,
                page: page
            })
            let keyword1 = this.state.cari1
            let keyword2 = this.state.cari2
            let keyword3 = this.state.cari3

            if (page) { } else { page = 1 }
            Promise.all([
                fetch(`http://localhost:8080/api/search/report/?keyword1=${keyword1}&keyword2=${keyword2}&keyword3=${keyword3}&limit=${this.state.limit}&page=${page}`),
                fetch('http://localhost:8080/api/search-counter/report/?keyword1=' + keyword1 + '&keyword2=' + keyword2 + '&keyword3=' + keyword3),
            ])
                .then(([response,
                    response2
                ]) => Promise.all([response.json(),
                response2.json()
                ]))
                .then(([json
                    , json2
                ]) => {

                    this.setState({
                        laporan: json,
                        count: Math.ceil(Number(json2.countSearch) / this.state.limit),
                        page: page
                    })
                    this.getReportPrint();

                })
        }


    }


    setValue = el => {
        if (el.target.name === "cari1" && this.state.cari2 === "" && this.state.cari3 === "" && el.target.value === "" ) {
            this.getReport(this.state.pageNow, this.state.limit);
            this.getCountReport();
        }
        else if  (el.target.name === "cari2" && this.state.cari1 === "" && this.state.cari3 === "" && el.target.value === "" ) {
                this.getReport(this.state.pageNow, this.state.limit);
                this.getCountReport(); 
        }
        else if  (el.target.name === "cari3" && this.state.cari1 === "" && this.state.cari2 === "" && el.target.value === "" ) {
            this.getReport(this.state.pageNow, this.state.limit);
            this.getCountReport(); 
    }
        this.setState({
            [el.target.name]: el.target.value,
        })

    }

    componentDidMount = () => {
        this.getReport(this.state.page, this.state.limit);
        this.getCountReport();
        // this.getReportPrint();

    }




    nav = () => {
        if (this.props.checkLogin === false) {
            this.props.history.push("/");
        } else if (this.props.checkLogin === true &&
            this.props.user.role === "Member") {
            this.props.history.push("/dashboard")
        }
    }

    render() {
        this.nav();
        console.log(this.state.count);
        console.log("page ini", this.state.page);
        console.log("cetak laporan", this.state.cetakLaporan);
        const { page, limit } = this.state

        return (
            <>
                <Container>
                    <Header />
                    <Div id="layoutSidenav">
                        <Nav>
                            <MenuTitle>
                                Menu
                            </MenuTitle>
                            <Link to="/dashboard-admin">
                                <Menu icon="fas fa-tachometer-alt">
                                    Beranda
                            </Menu>
                            </Link>
                            <Link to="/obat/admin">
                                <Menu icon="fas fa-prescription-bottle-alt">
                                    Obat
                                </Menu>
                            </Link>
                            <Link to="/pengguna">
                                <Menu icon="fas fa-user">
                                    Pengguna
                                </Menu>
                            </Link>
                            <Link to="/resep">
                                <Menu icon="fas fa-file-medical">
                                    Resep
                                </Menu>
                            </Link>
                            <Link to="/laporan">
                                <Active active="active">
                                    <Menu icon="fas fa-book">
                                        Laporan
                                    </Menu>
                                </Active>
                            </Link>
                        </Nav>
                        <Div id="layoutSidenav_content">
                            <Div className="container-table">
                                <Div className="judul-pengguna">
                                    <TitlePage icon="fas fa-user" >
                                        Laporan Pembelian
                                        </TitlePage>
                                </Div>
                                <Div className="row">
                                    <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                        <Input
                                            type="search"
                                            name="cari1"
                                            className="form-control"
                                            value={this.state.cari1}
                                            placeholder="Cari Nama pembeli"
                                            onChange={this.setValue}
                                        />
                                    </Div>
                                    <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                        <Input
                                            type="date"
                                            name="cari2"
                                            className="form-control"
                                            value={this.state.cari2}
                                            // placeholder="Cari Nama pembeli, Obat, Tanggal Pembelian"
                                            onChange={this.setValue}
                                        />
                                    </Div>
                                    <Div className="col-3 mb-2" style={{ padding: "inherit", marginLeft: "2vh" }}>
                                        <Input
                                            type="search"
                                            name="cari3"
                                            value={this.state.cari3}
                                            className="form-control"
                                            placeholder="Cari Obat"
                                            onChange={this.setValue}
                                        />
                                    </Div>
                                    <Div className="col-1">
                                        <Button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => { this.searchPagination() }}
                                        >
                                            <I className="fas fa-search" />
                                        </Button>
                                    </Div>
                                    <Div className="ml-5">

                                        <ReactToPrint
                                            trigger={() => {

                                                return (
                                                    <Button className="btn btn-primary">
                                                        <i className="fas fa-print"></i>
                                                    </Button>
                                                );
                                            }}
                                            content={() => this.componentRef}
                                        />
                                    </Div>
                                </Div>
                                {/* </Div> */}
                                <Div className="tabel">
                                    <table className="table table-bordered table-striped">
                                        <thead className="thead-dark">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama Pembeli</th>
                                                <th scope="col">Tanggal Transaksi</th>
                                                <th scope="col">Tanggal Konfirmasi</th>
                                                <th hidden="hidden" scope="col">Nama Obat</th>
                                                <th scope="col">Jumlah Barang</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Harga</th>
                                                <th style={{ textAlign: "center" }} scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.laporan.length === 0 ?
                                                <tr>
                                                    <td
                                                        colSpan="7"
                                                        align="center"
                                                    >Data kosong
                                                        </td>
                                                </tr>
                                                :

                                                this.state.laporan.map((a, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{(((page * limit) - limit + 1) + index)}</td>
                                                            <td>{a.name}</td>
                                                            <td>{a.tglTransaksi}</td>
                                                            <td>{a.tglKonfirmasi}</td>
                                                            <td hidden="hidden">
                                                                {a.obatList.map((b, index) => {
                                                                    return (

                                                                        <div key={index}>
                                                                            <p>
                                                                                - {b.namaObat}
                                                                            </p>
                                                                        </div>
                                                                    )
                                                                })
                                                                }
                                                            </td>
                                                            <td >{a.obatList.length} Jenis</td>
                                                            {a.statusLaporan === "Sukses" ?
                                                                <td><span className="badge badge-success">{a.statusLaporan}</span></td>
                                                                :
                                                                <td><span className="badge badge-danger">{a.statusLaporan}</span></td>
                                                            }
                                                            <td>{konversiRupiah(a.total)}</td>
                                                            <td>
                                                                <center>
                                                                    <Tooltip isi="Detail">
                                                                        <Button
                                                                            className="btn btn-secondary"
                                                                            dataToogle="modal"
                                                                            dataTarget="#obat"
                                                                            data-target="#exampleModal"
                                                                            onClick={() => { this.detailLaporanValue(a.idLaporan) }}
                                                                        ><I className="fas fa-info-circle"></I></Button>
                                                                    </Tooltip>
                                                                    <ReactTooltip />
                                                                </center>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    {this.state.laporan.length !== 0 &&
                                        <Div className="pagination" >
                                            <Pagination count={this.state.count} page={this.state.page}
                                                onChange={this.handleChange}
                                            />
                                        </Div>
                                    }

                                </Div>
                            </Div>
                            {/* </Main> */}
                            <Footer>
                                Copyright © Rilo 2021
                            </Footer>
                        </Div>
                    </Div>
                </Container>
                <Modal id="laporan">
                    <LaporanCetak ref={el => (this.componentRef = el)} laporan={this.state.cetakLaporan} />
                </Modal>
                <Modal id="obat">
                    <ModalHeader className="close">
                        Data Laporan
                    </ModalHeader>
                    <Div className="modal-body">
                        <Div style={{ padding: "0 11vh" }} className="container">
                            <Div className="row">
                                <Div className="col-md-3">
                                    ID Laporan
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.idLaporan}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Nama Pembeli
                                </Div>
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.nama}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Transaksi
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.tanggalTransaksi}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Tanggal Konfirmasi
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-12">
                                        : {this.state.tanggalKonfirmasi}
                                    </Div>
                                </Div>
                            </Div>
                            <Div className="row">
                                <Div className="col-md-3">
                                    Status
                                </Div>
                                {/* <Div className="col-md-1">:</Div> */}
                                <Div className="col-md-9 row">
                                    <Div className="col-md-8">
                                        : {this.state.status}
                                    </Div>
                                </Div>
                            </Div>
                            <br />
                            <Div className="table-responsive">
                                <table className="table table-bordered" style={{ width: "100%" }}>
                                    <thead>
                                        <tr>
                                            <th>Nama Obat</th>
                                            <th>Jenis Obat</th>
                                            <th>Kuantitas</th>
                                            <th>Harga</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.obatList.map((a, index) => {
                                            return (

                                                <tr key={index}>
                                                    <td>{a.namaObat}</td>
                                                    <td>{a.jenis}</td>
                                                    <td>{a.qty}</td>
                                                    <td>{konversiRupiah(a.hargaBarang)}</td>
                                                    <td>{konversiRupiah(a.totalHarga)}</td>
                                                </tr>
                                            )
                                        })
                                        }
                                        <tr>
                                            <td colSpan="4">Biaya Pengiriman</td>
                                            <td>{konversiRupiah(this.state.ongkir)}</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4">Total Pembayaran</td>
                                            
                                            <td>{this.state.bayar && konversiRupiah(this.state.bayar)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Div>
                        </Div>
                    </Div>
                    <Div className="modal-footer">
                        <Button type="button" className="btn btn-secondary" onClick={() => this.closeBtn()}>Tutup</Button>
                    </Div>
                </Modal>
            </>
        );
    }
}

const mapStateToProps = state => ({
    login: state.AReducer.dataUser,
    dataUser: state.UReducer.users,
    user: state.AReducer.dataUser,
    checkLogin: state.AReducer.isLogin


})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({

    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" })
})

export default (connect(mapStateToProps, mapDispatchToProps))(Laporan);
