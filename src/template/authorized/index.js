import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import { Login, Register } from '../../page';

class Authorized extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <Switch>
                <Login/>
                <Register/>
            </Switch>
         );
    }
}
 
export default Authorized;
