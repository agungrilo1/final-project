// import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import HeaderUmum from '../../../component/headerUmum';
// import gambar from '../../../asset/bg1.jpg';
// import Footer from '../../footer';
// import { connect } from 'react-redux';
// import {Menu, ModalHeader, Button, Active, Container, Nav, MenuTitle, Div, Modal} from '../../../component';
// import Pagination from '@material-ui/lab/Pagination';
// import swal from 'sweetalert';


// class Home extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             idObat:"",
//             namaObat:"",
//             stok:"",
//             harga:"",
//             dosis:"",
//             idJenisObat:"",
//             jenisObat:"",
//             limit:4,
//             page:1,
//             count:"",
//             obat:[],
//             total:"",
//             keranjang:{},
//             obatList:[]
//         }
//     }
//       //pagination
//       getCount = () => {
//         fetch('http://localhost:8080/api/count/', {
//             method: "get",
//             headers: {
//                 "Content-Type": "application/json; ; charset=utf-8",
//                 "Access-Control-Allow-Headers": "Authorization, Content-Type",
//                 "Access-Control-Allow-Origin": "*"
//             }
//         })
//             .then(response => response.json())
//             .then(json => {
//                 this.setState({
//                     count: Math.ceil(Number(json) / this.state.limit)
//                 });
//                 // console.log("count", this.state.count);
//                 // console.log("count", Number(json));
//             })
//             .catch((e) => {
//                 alert(e);
//             });
//     }
//     getTotal = () => {
//         fetch('http://localhost:8080/api/count/', {
//             method: "get",
//             headers: {
//                 "Content-Type": "application/json; ; charset=utf-8",
//                 "Access-Control-Allow-Headers": "Authorization, Content-Type",
//                 "Access-Control-Allow-Origin": "*"
//             }
//         })
//             .then(response => response.json())
//             .then(json => {
//                 this.setState({
//                     total:json
//                 });
//                 // console.log("count", this.state.count);
//                 // console.log("count", Number(json));
//             })
//             .catch((e) => {
//                 alert(e);
//             });
//     }

//     handleChange = (event, value) => {

//         this.setState({
//             page: value
//         })
//         console.log("value", value);
//         this.getObatListWithPage(value, 5);

//     }

//     getKeranjang = () => {
//         let id = this.props.user.idUser;
//         console.log("ini id", id);
//         fetch(`http://localhost:8080/api/keranjang/?idUser=${encodeURIComponent(id)}`, {
//             method: "get",
//             headers: {
//                 "Content-Type": "application/json; ; charset=utf-8",
//                 "Access-Control-Allow-Headers": "Authorization, Content-Type",
//                 "Access-Control-Allow-Origin": "*"
//             }
//         })
//             .then(response => response.json())
//             .then(json => {
//                 console.log("json",json);
//                 this.setState({
//                     keranjang: json
//                 });
//                 // if ($.isEmptyObject(this.state.keranjang) === true) {
//                 //     this.setState({
//                 //         obatList: []
//                 //     });
//                 // } else {
//                 //     this.setState({
//                 //         obatList: this.state.keranjang.obatList
//                 //     });
//                 // }
//                 // if (typeof json.errorMessage !== 'undefined') {
//                 //     swal("Gagal !", json.errorMessage, "error");
//                 // }
//             })
//             .catch((e) => {
//                 console.log(e);
//             });
//     };

//     createDetail=()=>{
//         const detail = {
//             idObat: this.state.idObat,
//             idKeranjang: this.state.keranjang.idKeranjang,
//         }
        
//             fetch(`http://localhost:8080/api/create-detail/`, {
//                 method: "POST",
//                 headers: {
//                     "Content-Type": "application/json; ; charset=utf-8",
//                     "Access-Control-Allow-Headers": "Authorization, Content-Type",
//                     "Access-Control-Allow-Origin": "*",
//                 },
//                 body: JSON.stringify(detail)
//             })
//             .then(resp => {
    
//                 if (!resp.ok) {
//                     return resp.json().then(text => {
//                         throw new Error(`${text.errorMessage}`)
//                     })
//                 } else {
//                     return resp.json();
    
//                 }
//             })
//             //menangkap data dari fetch
//             .then(json => {
//                 swal({
//                     title: `${json.errorMessage}`,
//                     icon: "success",
//                 })
//                     .then((value) => {
//                         // this.clearField();
//                         // $("#form-obat .close").click();
//                         // this.get();
//                         // this.getKeranjang();
    
//                     })
//             })
//             .catch((e) => {
//                 swal({
//                     title: "perhatian",
//                     text: `${e}`,
//                     icon: "error",
//                 });
    
//             });
        
//     }

//     getObatListWithPage = (page, limit) => {
//         if (page === undefined) { page = this.state.page }
//         else { }
//         fetch('http://localhost:8080/api/obat-pagination/?page=' + page + '&limit=' + this.state.limit + '', {
//             method: "get",
//             headers: {
//                 "Content-Type": "application/json; ; charset=utf-8",
//                 "Access-Control-Allow-Headers": "Authorization, Content-Type",
//                 "Access-Control-Allow-Origin": "*"
//             }
//         })
//             .then(response => response.json())
//             .then(json => {
//                 this.setState({ obat: json });
//             })
//             .catch((e) => {
//                 console.log(e);
//                 alert("Failed fetching data!!");
//             });
//     }

//     componentDidMount() {
//         this.getKeranjang();
//         this.getCount();
//         this.getObatListWithPage();
//         this.getTotal();
//         // this.getJenisObat();
//     }


//     //detail obat
//     detailObatValue = (idObat) => {
//         console.log(idObat);
//         const cari = this.state.obat.findIndex((idx) => idx.idObat === idObat);
//         const detail = this.state.obat[cari];
//         console.log(detail);
//         this.setState({
//             idObat: detail.idObat,
//             namaObat: detail.namaObat,
//             harga: detail.harga,
//             stok: detail.stok,
//             jenis: detail.jenis,
//             dosis: detail.dosis,
//             idJenisObat: detail.idJenisObat,

//         });
//     }
//     nav = () => {
//         if (this.props.checkLogin === true && this.props.user.role === "admin") {
//             this.props.history.push("/dashboard-admin");
//         } else if (this.props.checkLogin === true && this.props.user.role === "member") {
//             this.props.history.push("/dashboard");
//         }
//     }
//     render() {
//         this.nav();
//         return (
//             <>
//             <Container>

//                 <HeaderUmum />
//                 <div id="layoutSidenav">
//                     <div id="layoutSidenav_nav">
//                         <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
//                             <div className="sb-sidenav-menu bg-green">
//                                 <div className="nav">
//                                     <Div className="sb-sidenav-menu-heading">
//                                         Menu
//                                     </Div>
//                                     <Link to="/">
//                                         <Active active="active">
//                                             <Menu icon="fas fa-tachometer-alt">
//                                                 Home
//                                             </Menu>
//                                         </Active>
//                                     </Link>
//                                     <Link to="/keranjang"> 
//                                         <Menu icon="fas fa-shopping-cart">
//                                             Keranjang
//                                         </Menu>
//                                     </Link>
//                                     <Link to="/login">
//                                         <Menu icon="fas fa-user">
//                                             Login
//                                         </Menu>
//                                     </Link>
//                                 </div>
//                             </div>

//                         </nav>
//                     </div>
//                     <div id="layoutSidenav_content">
//                         <main>
//                         <div className="container-fluid">
//                                     <div className="row">
//                                         {
//                                             this.state.obat.map((a,index)=>{
//                                                 return(
//                                                     <div className="col-xl-3" key={index} >
//                                                         <div className="card obat m-b-30">
//                                                             <img  src={gambar} height={250} alt="Buku" />
//                                                             <div className="card-body">
//                                                                 <h5 className="mt-0 text-success">{a.namaObat} {a.stok === 0 &&
//                                                             <span className="badge badge-danger">Habis</span>} 
//                                                                 </h5>
                                                                
//                                                                     <p className="card-text">
//                                                                         Harga : Rp. {a.harga} 
//                                                                     </p>
//                                                             </div>
//                                                             <div style={{marginLeft:200, marginBottom:10}}>
                                                            
//                                                             <Button
//                                                                         className="btn btn-secondary" 
//                                                                         dataToogle="modal"
//                                                                         dataTarget="#obat"
//                                                                         data-target="#exampleModal"
//                                                                         onClick={() => { this.detailObatValue(a.idObat) }}
//                                                                     >
//                                                                     <i className="fas fa-info-circle"></i>
//                                                                     </Button>
//                                                             </div>
//                                                         </div>
//                                                     </div>
//                                                 )
//                                             })
//                                         }
//                                         <div 
//                                         className="pagination"
//                                         >
//                                             <div 
//                                             style={{marginLeft:60, 
//                                             marginTop:3}}
//                                             >
//                                                 <h4>{this.state.total} Obat</h4>
//                                         </div>
//                                             <div>
//                                             <Pagination count={this.state.count} page={this.state.page}
//                                                 onChange={this.handleChange}
//                                             />
//                                             </div>
                                        
//                                     </div>
                                        
//                                     </div>
                                   
//                                 </div>
//                         </main>
//                         <Footer>
//                             Copyright © Rilo 2021  
//                         </Footer>
//                     </div>
//                 </div>
//             </Container>
//             <Modal id="obat">
//                     <ModalHeader className="close">
//                         Detail Obat
//                     </ModalHeader>
//                     <Div className="modal-body">
//                         <Div style={{ padding: "0 11vh" }} className="container">
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     ID Obat
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.idObat}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     Nama Obat
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.namaObat}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     Stok
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.stok}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     Harga
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.harga}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     Jenis Obat
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.jenis}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                             <Div className="row">
//                                 <Div className="col-md-2">
//                                     Dosis
//                                 </Div>
//                                 <Div className="col-md-1">:</Div>
//                                 <Div className="col-md-9 row">
//                                     <Div className="col-md-12">
//                                         {this.state.dosis}
//                                     </Div>
//                                 </Div>
//                             </Div>
//                         </Div>
//                     </Div>
//                     <Div className="modal-footer">
//                         {this.state.stok === 0 ?
//                         <Button type="button" disabled="disabled" className="btn btn-warning text-white" >
//                         <i className="fas fa-cart-plus"></i> Stok Habis
//                         </Button>
//                         :
//                         <Button type="button" className="btn btn-warning text-white" onClick={() => this.createDetail()}>
//                         <i className="fas fa-cart-plus"></i> Masukkan kedalam keranjang
//                         </Button>
//                         }
                       
//                     </Div>
//                 </Modal>
//             </>
//         );
//     }
// }

// //ketika mengambil data dari luar kelas
// const mapStateToProps = state => ({
//     checkLogin: state.AReducer.isLogin,
//     // dataUser: state.UReducer.users,
//     user: state.AReducer.dataUser

// })
// //mengubah data kereducer
// const mapDispatchToProps = dispatch => ({
//     // submitLogin: (data) => dispatch({ type: "LOGIN_SUCCESS", payload: data }),

// })

// export default (connect(mapStateToProps, mapDispatchToProps))(Home);