import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Login, Register, ChangePass} from '../../../page';
import { ObatMember, Profil } from '../../../page/member';
import Dashboard from '../../../page/member/dashboard';
import KeranjangUmum from '../../../page/umum/keranjang';
import { KeranjangMember } from '../../../page/member';
import Home from '../home';
import { User,  DashboardAdmin, ObatAdmin, Laporan, Resep } from '../../../page/admin';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <>
                <Switch>
                    {/* login dan register */}
                    <Route exact path="/" component={props => <Login {...props} />} />
                    <Route path="/register" component={props => <Register {...props} />} />
                    <Route path="/password" component={props => <ChangePass {...props} />} />
                    {/* halaman admin */}
                    <Route path="/dashboard-admin" component={props => <DashboardAdmin {...props} />} />
                    <Route path="/pengguna" component={props => <User {...props} />} />
                    <Route path="/obat/admin" component={props => <ObatAdmin {...props} />} />
                    <Route path="/laporan" component={props => <Laporan {...props} />} />
                    <Route path="/resep" component={props => <Resep {...props} />} />
                    {/*halaman member */}
                    <Route path="/dashboard" component={props => <Dashboard {...props} />} />
                    <Route path="/obat" component={props => <ObatMember {...props} />} />
                    <Route path="/keranjang-member" component={props => <KeranjangMember {...props} />} />
                    <Route path="/profil" component={props => <Profil {...props} />} />
                </Switch>
            </>
         );
    }
}
 
export default Body;