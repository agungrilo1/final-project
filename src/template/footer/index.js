import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const{children}=this.props
        return ( 
            <footer className="py-2 bg-light mt-auto">
                <div className="container-fluid">
                    <div className="d-flex align-items-center justify-content-between small">
                        <div className="text-muted">{children}</div>
                    </div>
                </div>
            </footer>
         );
    }
}
 
export default Footer;