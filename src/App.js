import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import Body from './template/umum/body';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() { 
    return ( 
      <>
      <Router>
        <Body/>
      </Router>
      </>
     );
  }
}
 
export default App;


