import React, { Component } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import $ from 'jquery';
import {Link} from 'react-router-dom';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { 
    Label,
    Input,
    Button 
    } from '../../component';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            kataSandi: "",
            kataSandiLama:"",
            konfirmasiKataSandi: "",
            passType: true,
            passType2: true,
            passType3: true,
         }
    }

    clearField=()=>{
        this.setState({
            kataSandi: "",
            kataSandiLama:"",
            konfirmasiKataSandi: "",
        })
    }
    setValue = el => {
        this.setState({
            [el.target.name]: el.target.value,
            disabled: !this.state.disabled
        })

    }

    doChangePass = () => {
        let object = this.state;
        let regKataSandi = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*[&%$]).{6,8}$/;
        console.log("pas :", regKataSandi.test(object.kataSandi));
        console.log("props", this.props.user);
        const regpas = regKataSandi.test(object.kataSandi);
        //true lanjut
        //false block
        if (object.kataSandiLama === "" || object.kataSandi === "" || object.konfirmasiKataSandi === "") {
            swal({
                title: "Gagal !!",
                text: "Kata sandi baru dan konfirmasi kata sandi harus diisi",
                icon: "warning"
            })
        } else if(object.kataSandiLama !== this.props.user.password){
            swal({
                title: "Gagal !!",
                text: "Kata sandi lama tidak sesuai",
                icon: "warning"
            }) 
        } else if(object.kataSandi === this.props.user.password){
            swal({
                title: "Gagal !!",
                text: "Tidak boleh menggunakan kata sandi lama",
                icon: "warning"
            }) 
        } else if (regpas === false) {
            swal({
                title: "Gagal !!",
                text: "Kata sandi harus terdiri dari 6 sampai 8 alfanumerik dan minimal 1 huruf kapital",
                icon: "warning"
            })
        }else if (object.kataSandi !== object.konfirmasiKataSandi) {
            swal({
                title: "Gagal !!",
                text: "konfirmasi kata sandi tidak sesuai dengan kata sandi baru",
                icon: "warning"
            })
        } else {
            swal({
                title: "Apakah anda yakin ingin mengganti kata sandi",
                text: "Setelah anda mengubah kata sandi anda akan diarahkan ke halaman awal masuk",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Kata sandi berhasil diubah", {
                            icon: "success",
                        });
                        const changePass = {
                            idUser: this.props.user.idUser,
                            password: this.state.kataSandi
                        }
                        fetch(`http://localhost:8080/api/change-password/`, {
                            method: "put",
                            headers: {
                                "Content-Type": "application/json; ; charset=utf-8",
                                "Access-Control-Allow-Headers": "Authorization, Content-Type",
                                "Access-Control-Allow-Origin": "*"
                            },
                            body: JSON.stringify(changePass)
                        })
                            .then((response) => response.json())
                            .then((json) => {
                                console.log("ini json", json);
                                console.log("ini json", json.errorMessage);
                                if (json.errorMessage === "undefined") {
                                    swal("Gagal !", json.errorMessage, "error");

                                } else {

                                    // this.props.history.push("/profil")
                                    // this.props.logoutEvent()
                                    
                                    swal({
                                        icon: "success",
                                        title: "Berhasil"
                                    })
                                    .then((value) => {
                                        $("#kata-sandi .close").click();
                                        console.log("object", value);
                                            this.props.logoutEvent();
                                            // this.props.history.push("/")
                                            
                                        })
                                }
                            })
                            .catch((e) => {
                                console.log(e);
                                swal("Gagal !", "Gagal mengambil data", "error");
                            });
                    }
                });

        }
    }
    passClick = () => {
        this.setState({
            passType: !this.state.passType
        })
    }

    passClick2 = () => {
        this.setState({
            passType2: !this.state.passType2
        })
    }
    passClick3 = () => {
        this.setState({
            passType3: !this.state.passType3
        })
    }

    doLogout = () => {
        swal({
            title: "Apakah anda yakin ingin keluar?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                swal("Anda berhasil keluar", {     
                    icon: "success",
                    title:"Terima Kasih"
                });
                this.props.logoutEvent();
                 
            } 
          });
      }
    
    render() { 
        console.log("state", this.state.kataSandi);
        return ( 
            <>
            <nav id="header" className="sb-topnav navbar navbar-expand navbar-success bg-green">
                <Link to={this.props.user.role === "Umum"?
            
            "/dashboard-admin"
            :
            "/dashboard"
            }>
                <i id="logo" className="fas fa-mortar-pestle">
                </i>

                <div className="navbar-brand" >
                    Apotek Sehat Sentosa
                </div>
                </Link>
                <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                       
                    </form>
                <ul className="navbar-nav ml-auto ml-md-0">
                        <li className="nav-item dropdown">
                            <a className="nav-link acc dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fas fa-user fa-fw" /></a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                <div className="dropdown-item" >
                                    {this.props.user.role === "Umum" ?
                                     <div>
                                     {this.props.login.name}
                                    </div>
                                    :
                                    <Link to="/profil">
                                        <div style={{color:"black",textDecoration: 'none'}}> 
                                            {this.props.login.name}
                                        </div>
                                    </Link>
                                   
                                    }
                                    
                                </div>
                                    <div className="dropdown-divider" />
                                    {this.props.login.role === "Admin" ?
                                    <div className="dropdown-item" >
                                            <Button 
                                            className="change-password-admin-text" 
                                            dataToogle="modal"
                                            dataTarget="#kata-sandi" 
                                            
                                            onClick={()=>{this.clearField()}}
                                            style={{
                                                border: "none", 
                                                textDecoration:"none",
                                                backgroundColor : "Transparent",
                                                backgroundRepeat: "no-repeat",
                                                border:"none",
                                                marginLeft: "-1vh",
                                                cursor:"pointer",
                                                overflow: "hidden",
                                                outline:"none",
                                                marginRight:"5vh"
                                            }}>Ubah Kata Sandi
                                            </Button>
                                            {/* <div className="dropdown-divider" /> */}
                                    </div>
                                        :
                                        <div>

                                        </div>
                                    }
                                
                                <div className="dropdown-item" style={{
                                cursor: "pointer" ,  color: "black", 
                                }}  
                                    onClick={()=>{this.doLogout()}}>
                                        {/* <div className="dropdown-divider" /> */}
                                    Keluar
                                    </div>
                            </div>
                        </li>
                    </ul>         
            </nav>
            {/* Modal */}
            <div className="modal fade" id="kata-sandi" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Ubah Kata Sandi</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="input-group mb-3">
                                    <input type={this.state.passType?"password" : "text"} name="kataSandiLama" className="form-control" placeholder="Masukkan Kata Sandi Lama" aria-label="Username" aria-describedby="basic-addon1" onChange={this.setValue}/>
                                    <button onClick={() => { this.passClick() }} className="input-group-text" id="basic-addon1">{this.state.passType ? <FaEyeSlash /> : <FaEye />}</button>
                                </div>
                                <div className="input-group mb-3">
                                    <input type={this.state.passType2?"password" : "text"} name="kataSandi" className="form-control" placeholder="Masukkan Kata Sandi Baru" aria-label="Username" aria-describedby="basic-addon1" onChange={this.setValue}/>
                                    <button onClick={() => { this.passClick2() }} className="input-group-text" id="basic-addon1">{this.state.passType2 ? <FaEyeSlash /> : <FaEye />}</button>
                                </div>
                                <div className="input-group mb-3">
                                    <input type={this.state.passType3?"password" : "text"} name="konfirmasiKataSandi" className="form-control" placeholder="Masukkan Konfirmasi Kata Sandi" aria-label="Username" aria-describedby="basic-addon1" onChange={this.setValue}/>
                                    <button onClick={() => { this.passClick3() }} className="input-group-text" id="basic-addon1">{this.state.passType3 ? <FaEyeSlash /> : <FaEye />}</button>
                                </div>

                                {/* <Label >Kata Sandi Lama</Label>
                                <Input type="password" name="kataSandiLama" className="form-control" placeholder="Masukkan Kata Sandi" onChange={this.setValue} />
                                <Label >Kata Sandi Baru</Label>
                                <Input type="password" name="kataSandi" className="form-control" placeholder="Masukkan Kata Sandi" onChange={this.setValue} />
                                <Label>Konfirmasi Kata Sandi Baru</Label>
                                <Input type="password" name="konfirmasiKataSandi" className="form-control" placeholder="Konfirmasi Kata Sandi" onChange={this.setValue} /> */}

                            </div>
                            <div className="modal-footer">
                                {/* <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button> */}
                                <Button type="button" className="btn btn-primary" onClick={() => this.doChangePass()}>Simpan</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
         );
    }
}
 
//ketika mengambil data dari luar kelas
const mapStateToProps = state => ({
    login:state.AReducer.dataUser,
    user: state.AReducer.dataUser,
})
//mengubah data kereducer
const mapDispatchToProps = dispatch => ({
    logoutEvent: () => dispatch({ type: "LOGOUT_SUCCESS" }),
    
})

export default (connect(mapStateToProps, mapDispatchToProps))(Header);