import React, { Component } from 'react';

class MenuTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children} = this.props
        return ( 
            <div className="sb-sidenav-menu-heading">
                {children}
            </div>
         );
    }
}
 
export default MenuTitle;