import React, { Component } from 'react';

class FormGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children, className} = this.props
        return ( 
            <div className={className}>
                {children}
            </div>
         );
    }
}
 
export default FormGroup;