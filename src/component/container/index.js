import React, { Component } from 'react';

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const{ children } = this.props
        return ( 
            <div className="sb-nav-fixed">
                {children}
            </div>
         );
    }
}
 
export default Container;