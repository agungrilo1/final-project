import React, { Component } from 'react';

class ModalHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children, className} = this.props
        return ( 
            <>
                <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">{children}</h5>
                        <button type="button" className={className} data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
            </>
         );
    }
}
 
export default ModalHeader;