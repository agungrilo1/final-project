import React, { Component } from 'react';


class TextArea extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const { value, type, name, className, placeholder, onChange} = this.props
        return ( 
                <textarea type={type} value={value} name={name} className={className} placeholder={placeholder} onChange={onChange} />
         );
    }
}
 
export default TextArea;