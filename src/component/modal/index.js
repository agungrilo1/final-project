import React, { Component } from 'react';

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const { children,id }=this.props
        return ( 
            <>
             <div className="modal fade" id={id} tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content modal-lg">
                        {children}
                    </div>
                </div>
            </div>
            </>
         );
    }
}
 
export default Modal;