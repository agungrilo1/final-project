import React, { Component } from 'react';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {  children,icon} = this.props
        return ( 
            

            <div className="nav-link">
                <div className="sb-nav-link-icon ">
                    <i style={{color:"white"}} className={icon} />
                </div>
                    {children}
            </div>
            
         );
    }
}
 
export default Menu ;