import React, { Component } from 'react';

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() {
        const {children}=this.props 
        return (          
                <div id="layoutSidenav_nav">
                    <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div className="sb-sidenav-menu bg-green">
                            <div className="nav">
                                {children}
                            </div>
                        </div>
                    </nav>
                </div>
         );
    }
}
 
export default Nav;