import React, { Component } from 'react';

class FormRow extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const { children } = this.props
        return ( 
            <>
                
                    <div className="col-md-6">
                        <div className="form-group">
                            {children}
                        </div>
                    </div>
                
            </>
         );
    }
}
 
export default FormRow;