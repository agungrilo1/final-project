import React, { Component } from 'react';

class I extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        
        return (
            <i className={this.props.className} onClick={this.props.onClick} style={this.props.style}>{this.props.children}</i>
          );
    }
}
 
export default I;