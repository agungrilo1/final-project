import React, { Component } from 'react';

class Option extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children, value, className, key} = this.props
        return ( 
            <>
                <option value={value} key={key} className={className}>
                    {children}
                </option>
            </>
         );
    }
}
 
export default Option;