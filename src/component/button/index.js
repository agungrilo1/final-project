import React, { Component } from 'react';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children,hidden, dataToogle, dataTarget, value, type, name, id, className, dataDismiss, ariaLabel, onClick, style, disabled}=this.props
        return ( 
            <div>
                <button hidden={hidden} data-toggle={dataToogle} disabled={disabled} data-target={dataTarget} type={type} aria-label={ariaLabel} data-dismiss={dataDismiss} value={value} id={id} name={name} className={className} style={style}  onClick={onClick}>{children}</button>
            </div>
         );
    }
}
 
export default Button;