import Input from './input';
import Label from './label';
import Button from './button';
import Menu from "./menu";
import P from './p';
import Main from './main';
import I from './i';
import Tambah from './tambah';
import Img from './img';
import H3 from './h3';
import H5 from './h5';
import Background from './background';
import Layout from './layout';
import Form from './form';
import MenuTitle from './menuTitle';
import Nav from './nav';
import TitlePage from './titlePage';
import Active from './active';
import Container from './container';
import Header from './header';
import FormGroup from './formGroup';
import Div from './div';
import FormRegistration from './formRegistration';
import TextArea from './textArea';
import Modal from './modal';
import ModalHeader from './modalHeader';
import FormRow from './formRow';
import Option from './option';
import Select from './select';
import Tooltip from './tooltip';


export { Input,
         Header,
         Label,
         Button,
         Menu,
         P,
         Main,
         I,
         Tambah,
         Img,
         H3,
         H5,
         Background,
         Layout,
         Form,
         MenuTitle,
         Nav,
         TitlePage,
         Active,
         Container,
         FormGroup,
         Div,
         FormRegistration,
         TextArea,
         Modal,
         ModalHeader,
         FormRow,
         Select,
         Option,
         Tooltip
         
        }