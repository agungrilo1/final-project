import React, { Component } from 'react';

class Div extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {style,className, id, children}= this.props
        return ( 
            <div style={style} className={className} id={id}>
                {children}
            </div>
         );
    }
}
 
export default Div;