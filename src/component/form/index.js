import React, { Component } from 'react';
import login from '../../asset/apotek2.png';


class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const {children} = this.props
        return (
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7">
                            <div className="card shadow-lg border-0 rounded-lg mt-5">
                                <div className="card-header"><img id="login" alt="" src={login} /></div>
                                <div className="card-body">
                                    {children}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default Form;