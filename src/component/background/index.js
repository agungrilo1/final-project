import React, { Component } from 'react';

class Background extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children} = this.props
        return ( 
            <div className="background">{children}</div>
         );
    }
}
 
export default Background;
