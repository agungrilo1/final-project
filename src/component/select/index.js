import React, { Component } from 'react';

class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children, name, value, onChange, className} = this.props
        return ( 
            <>
                <select name={name} value={value} onChange={onChange} className={className}  aria-label="Default select example">
                    {children}
                </select>
            </>
         );
    }
}
 
export default Select;