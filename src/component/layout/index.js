import React, { Component } from 'react';

class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children} = this.props
        return ( 
            <div id="layoutAuthentication">
                    <div id="layoutAuthentication_content">
                        {children}
                    </div>
            </div>
         );
    }
}
 
export default Layout;