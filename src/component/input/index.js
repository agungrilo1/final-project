import React, { Component } from 'react';


class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const { value, pattern, min, type, name, id, className, placeholder, cheked, onChange, disabled, max} = this.props
        return ( 
            
                <input 
                    pattern={pattern} 
                    type={type} 
                    min={min} 
                    value={value} 
                    id={id} 
                    max={max}
                    name={name} 
                    disabled={disabled} 
                    className={className} 
                    placeholder={placeholder} 
                    onChange={onChange}
                    cheked={cheked} />
            
         );
    }
}
 
export default Input;