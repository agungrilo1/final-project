import React, { Component } from 'react';


class Label extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {children, className, hidden}=this.props
        return ( 
            
                <label className={className} hidden={hidden}>{children}</label>
            
         );
    }
}
 
export default Label;
