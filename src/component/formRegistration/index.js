import React, { Component } from 'react';



class FormRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const {children} = this.props
        return (
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-10 mb-5">
                            <div className="card shadow-lg border-0 rounded-lg mt-5">
                            <div className="card-header"><h3 className="text-center font-weight-light my-4">Registrasi</h3></div>
                            <div className="card-body">
                                {children}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default FormRegistration;