import React, { Component } from 'react';

class TitlePage extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const{children} = this.props
        return ( 
            <div className="judul">
                <h2 >
                    {/* <i className={icon} id={id} /> */}
                    <div className="title-table">
                        {children}
                    </div>
                </h2>
            </div>
         );
    }
}
 
export default TitlePage;