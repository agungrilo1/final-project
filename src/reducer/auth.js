let defaultState = {
    isLogin:false,
    dataUser:{
        idUser:"",
        role:"",
        name:"",
        password:""   
    }
}

const authReducer = (state = defaultState, action) => {
    console.warn("state:", state);
    console.warn("action:", action);
    console.log("action ini",action.payload);

  
    
    switch (action.type) {
        case "LOGIN_SUCCESS":
            console.log(action.payload.user);
            return {
                isLogin: true,
                dataUser:
                {
                    idUser: action.payload.user.idUser,
                    name : action.payload.user.name,
                    role: action.payload.user.role,
                    password : action.payload.user.password
                }
            }
        case "LOGOUT_SUCCESS":
            return defaultState

        default:
            return state
    }
}

export default authReducer