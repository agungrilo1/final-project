import AuthReducer from './auth';
import UserReducer from './user';

import { combineReducers } from "redux";

const reducer = combineReducers({
    AReducer: AuthReducer,
    UReducer: UserReducer
});

export default reducer